
[Verse 1]

G                Bm        Esus2
You escape like a runaway train
G                           Bm     Esus2
Off the tracks and down again
G                                    Bm             Esus2
My heart's beating like a steamboat tugging.
G     Bm    D  Bm - G   Bm  
D      Bm
All your burdens, on my shoulders.


[Chorus]

(Bm)         G     D      A
In the morning, I'll rise.
(A)           G     D      A
In the mourning, I'll let you die.
(A)      G   D   Em
In the mourning,
(Em)   G  Bm  D
All my worry.


[Verse 2]

G                                          Bm             Esus2
And now there's nothing but time that's wasted
G                                   Bm        Esus2
And words that have no backbone
G                               Bm                  Esus2
Now it seems like the whole world’s waiting
G  Bm   D   Bm -- G   Bm  D   Bm
Can you hear the echoes fading?


[Chorus]

(Bm)    G            D         A
In the morning, I’ll rise.
(A)      G          D              A
In the mourning, I’ll let you die
(A)     G     D      Em
In the mourning,
          G  Bm D
All my sorry.


[Verse 3]

 (D)  Bm       G         D
And it takes all my strength
(D)                  Em               Bm                      
      A
Not to dig you up from the ground, in which you lay.
(A)                          Em
The biggest part of me
(Em)             D
You were the greatest thing
(D)   Bm                        Em
And now you are just a memory
(Em) G   Bm   D
To let go of.


[Chorus]

(D)    G            D         A
In the morning, I’ll rise.
(A)      G          D              A
In the mourning, I’ll let you die
(A)     G     D      Em
In the mourning,
          G   Bm  D
All my sorry.