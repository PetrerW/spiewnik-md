#!/bin/bash

# Step 1: Collect filenames from the "txt" folder and store them in the "songs" array
songs=(txt/*.txt)

# Step 2: Create the "songbook.md" markdown file
songbook="songbook.md"
> "$songbook"  # Clear the file if it already exists

# Step 3: Generate the markdown table of contents
echo "# Songbook" >> "$songbook"
echo >> "$songbook"
echo "## Table of Contents" >> "$songbook"
echo >> "$songbook"

# Loop through the filenames in alphabetical order and add them to the table of contents
for song in "${songs[@]}"; do
  filename=$(basename "$song")
  title="${filename%.txt}"
  title_without_hyphen=$(echo "$title" | sed 's/ - /---/g')
  link=$(echo "$title_without_hyphen" | tr '[:upper:]' '[:lower:]' | sed 's/ /-/g')  # Generate link using lowercase and hyphens

  echo "- [$title](#user-content-$link)" >> "$songbook"
done

echo >> "$songbook"

# Step 4: Write the contents of each file in a markdown code block
for song in "${songs[@]}"; do
  filename=$(basename "$song")
  title="${filename%.txt}"
  link=$(echo "$title" | tr '[:upper:]' '[:lower:]' | sed 's/ /-/g')  # Generate link using lowercase and hyphens

  echo "## $title" >> "$songbook"
  echo >> "$songbook"
  echo "[Back to Table of Contents](#user-content-table-of-contents)" >> "$songbook"  # Add link back to the table of contents
  echo >> "$songbook"
  echo -e '\n```\n' >> "$songbook"
  cat "$song" >> "$songbook"
  echo -e '\n```\n' >> "$songbook"
  echo >> "$songbook"
done
