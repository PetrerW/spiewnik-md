# Songbook

## Table of Contents

- [Aerosmith - Cryin](#user-content-aerosmith---cryin)
- [Animals - House of rising sun](#user-content-animals---house-of-rising-sun)
- [Aretha Franklin - Natural Woman](#user-content-aretha-franklin---natural-woman)
- [Artur Rojek - Syreny](#user-content-artur-rojek---syreny)
- [Avenged Sevenfold - So far away](#user-content-avenged-sevenfold---so-far-away)
- [Birdy - Deep End](#user-content-birdy---deep-end)
- [Coldplay - Fix you DE](#user-content-coldplay---fix-you-de)
- [Coldplay - Fix you](#user-content-coldplay---fix-you)
- [Coldplay - Hymn for the weekend](#user-content-coldplay---hymn-for-the-weekend)
- [Coldplay - Violet hill](#user-content-coldplay---violet-hill)
- [Dua Lipa - Levitating](#user-content-dua-lipa---levitating)
- [Dżem - Partyzant](#user-content-dżem---partyzant)
- [Frank Sinatra - Fly Me to the Moon](#user-content-frank-sinatra---fly-me-to-the-moon)
- [Frank Sinatra - The lady is a tramp](#user-content-frank-sinatra---the-lady-is-a-tramp)
- [Green Day - Buolevard of broken dreams](#user-content-green-day---buolevard-of-broken-dreams)
- [Grzegorz Turnau - Naprawdę nie dzieje się nic](#user-content-grzegorz-turnau---naprawdę-nie-dzieje-się-nic)
- [Happysad - Bez znieczulenia](#user-content-happysad---bez-znieczulenia)
- [Happysad - Mów mi dobrze](#user-content-happysad---mów-mi-dobrze)
- [Hillsong United Oceans](#user-content-hillsong-united-oceans)
- [If I Stay - Heart Like Yours - Willamette Stone](#user-content-if-i-stay---heart-like-yours---willamette-stone)
- [I hear your voice - Why are you here](#user-content-i-hear-your-voice---why-are-you-here)
- [Iron Maiden - Fear of the dark](#user-content-iron-maiden---fear-of-the-dark)
- [Janusz Radek - Kiedy U-kochanie](#user-content-janusz-radek---kiedy-u-kochanie)
- [John Legend - All of me](#user-content-john-legend---all-of-me)
- [Kombii - Ślad](#user-content-kombii---Ślad)
- [Lianne la havas - Ghost](#user-content-lianne-la-havas---ghost)
- [Lianne La Havas - Green and Gold](#user-content-lianne-la-havas---green-and-gold)
- [Lianne la havas - is your love big enough](#user-content-lianne-la-havas---is-your-love-big-enough)
- [Lianne la Havas - No room for doubt](#user-content-lianne-la-havas---no-room-for-doubt)
- [Lianne la havas - Wonderful](#user-content-lianne-la-havas---wonderful)
- [Linkin Park - Final Masquerade](#user-content-linkin-park---final-masquerade)
- [Linkin Park - In the end](#user-content-linkin-park---in-the-end)
- [Lor - Nikt](#user-content-lor---nikt)
- [Marek Grechuta - Ocalić od zapomnienia](#user-content-marek-grechuta---ocalić-od-zapomnienia)
- [Paramore - In The mourning](#user-content-paramore---in-the-mourning)
- [Sam Brown - Stop](#user-content-sam-brown---stop)
- [Snarky Puppy, Laura Mvula - Sing to the Moon](#user-content-snarky-puppy,-laura-mvula---sing-to-the-moon)
- [SOAD - Lonely Day](#user-content-soad---lonely-day)
- [SOAD - Roulette](#user-content-soad---roulette)
- [SOAD - Toxicity](#user-content-soad---toxicity)
- [Son Lux - Easy](#user-content-son-lux---easy)
- [Sportfreunde Stiller - Kompliment](#user-content-sportfreunde-stiller---kompliment)
- [Tom Odell - Heal 2 półtony w dół](#user-content-tom-odell---heal-2-półtony-w-dół)
- [Tom Odell - Heal](#user-content-tom-odell---heal)
- [Willamette Stone - I never wanted to go](#user-content-willamette-stone---i-never-wanted-to-go)

## Aerosmith - Cryin

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]
Bb5 C5 F5 G5
Bb5 C5 F5 G5
Bb5 C5 F5 G5
Bb5 C5 F5 G5   Bb5
 
[Verse]
A              E
   there was a time
F#m              C#m
   when I was so broken-hearted
D              A         E
   love wasn't much of a friend of mine
A                  E
   the tables have turned, yeah
F#m                   C#
   'cause me and them ways have parted
D               A            E
   that kind of love was the killin' kind -- listen
 
[Prechorus]
G        D           G             D
   all I want is someone I can't resist
C              G                    E
   I know all I need to know by the way that I got kissed
 
[Chorus]
         A             E
   I was cryin' when I met you
           F#m          D
   now I'm tryin' to forget you
A          E          D
   love is sweet misery
         A              E
   I was cryin' just to get you
           C#m            D
   now I'm dyin' 'cause I let you
A              E           D
   do what you do, down on me
 
[Bridge]
Bb5 C5 F5 G5
             now there's not even breathin' room
Bb5 C5 F5 G5
             between pleasure and pain
Bb5 C5 F5 G5
             yeah, you cry when we're makin' love
Bb5 C5 F5 G5                     Bb5
             must be one and the same
 
[Verse]
A               E
   it's down on me
   F#m           C#m
   yeah I got to tell you one thing
D                    A            E
   that's been on my mind, girl I gotta say
A                    E
   we're partners in crime
F#m          C#
you got that certain something
D                   A           E
   what you give to me takes my breath away
 
[Prechorus]
           G               D             G               D
   now the word out on the street is the devil's in your kiss
C                   G                   E
   if our love goes up in flames it's a fire I can't resist
 
[Chorus]
         A             E
   I was cryin' when I met you
           F#m          D
   now I'm tryin' to forget you
A               E          D
   your love is sweet misery
         A              E
   I was cryin' just to get you
           C#m            D
   now I'm dyin' 'cause I let you
A              E     D
   do what you do to me
 
[Solo]
Bb5 C5 F5 G5
Bb5 C5 F5 G5
Bb5 C5 F5 G5
Bb5 C5 F5 G5   Bb5
 
[Alt. Prechorus]
          Eb             Bb
   'cause what you got inside
         Eb                     Bb
   ain't where your love should stay
       Ab                    Eb
   yeah, our love, sweet love, ain't love
           E
   till ya give your heart away
 
[Chorus]
         A             E
   I was cryin' when I met you
           C#m          D
   now I'm tryin' to forget you
A               E          D
   your love is sweet misery
         A              E
   I was cryin' just to get you
           F#m      D
   now I'm dyin' to let you
A              E                       D
   do what you do what you do down to...
 
[Solo]
A  E  C#m  D  A  E  D
A  E  C#m  D  A  E  D
 
[Chorus to fade]
A  E  C#m  D  A  E  D


```


## Animals - House of rising sun

[Back to Table of Contents](#user-content-table-of-contents)


```

[Verse 1] 

      Am   C        D          F
There is a house in New Orleans,
      Am       C      E7
They call the "Rising Sun",
     Am       C       D           F
It's been the ruin of many a poor boy,
    Am     E         Am    C    D    F
And God, I know, I'm one.

      Am   C        D          F
My mother was a tailor,
      Am       C      E7
She sewed my new blue jeans,
     Am       C       D           F
My father was a gambling man,
    Am     E         Am    C    D    F
Down in New Orleans.

      Am   C        D          F
Now the only thing a gambler needs,
      Am       C      E7
Is a suitcase and a trunk,
     Am       C       D           F
And the only time, he's satisfied, is when
    Am     E         Am    C    D    F
He's on a drunk


[Solo]


[Verse 2]

      Am   C        D          F
Oh, mother, tell your children
      Am       C      E7
Not to do what I have done -
     Am       C       D           F
Spend your lives in sin and misery
    Am     E         Am    C    D    F
In the House of Rising Sun

      Am   C        D          F
Well, I've  got one foot on the platform,
      Am       C      E7
The other's on the train,
     Am       C       D           F
I'm going back to New Orleans,
    Am     E         Am    C    D    F
to wear that ball and chain.

      Am   C        D          F
Well, there is a house in new Orleans
      Am       C      E7
They call the "Rising Sun",
     Am       C       D           F
And it's been the ruin of many a poor boy,
    Am     E         Am    C    D    F
And God I know i'm one
```


## Aretha Franklin - Natural Woman

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]

C   Fmaj  C   G

[Verse]

G                  D/F#                    
Looking out on the morning rain,
F                  C/E    C/E  G/D     Am
I used to feel uninspired
G                              D/F#
And when I knew I had to face another day,
F                        C/E    C/E  G/D     Am
Lord, it made me feel so tired
Bm              Am       Bm           Am
Before the day I met you, life was so unkind,
    Bm               C                  Am  
but your love was the key to my peace of mind

[Chorus]

       D           G     C           G    
'Cause you make me feel, you make me feel,
C           G    C    G Em     Am    D
you make me feel like a natural woman,

[Verse]

G                       D/F#
When my soul was in the lost-and-found,
F                 C/E    C/E  G/D     Am
you came along to claim it
G                           D/F#
I didn't know just what was wrong with me,
F                        C/E    C/E  G/D     Am
till your kiss helped me name it
Bm               Am        Bm                Am
Now I'm no longer doubtful, of what I'm living for,
       Bm                  C                  Am  
'cause if I make you happy, I don't need to do more

[Chorus]

       D           G     C           G    
'Cause you make me feel, you make me feel,
C           G    C    G Em     Am    D
you make me feel like a natural woman,

[Break]

G                     F                                  F  F  C/E
Oh, baby, what you've done to me, (what you've done to me)
G                   F                       F  F  C/E              
you make me feel so good inside (good inside)
Am         Gm7/F                   C
And I just want to be (want to be) close to you,
    G/B             Am  
you make me feel so alive

[Outro]

C           G     C           G
You make me feel, you make me feel,
C           G    C    G Em     Am    
you make me feel like a natural woman,
D           G     C           G
You make me feel, you make me feel,
C           G    C    G Em     Am    
you make me feel like a natural woman,
  D7      G
a natural woman.
```


## Artur Rojek - Syreny

[Back to Table of Contents](#user-content-table-of-contents)


```

F#
Patrze jak miesza sie  
D#m  F#  B          
to czego nie ma i to co jest
F#     
A potem kiedy mowie wprost 
B  F#  B      
milczysz i odwracasz wzrok       

F#
Tysiac wypitych razem kaw   
B  F#  B     
glupi myslalem, ze cie znam 
F#     
W rzucaniu kamieniami slow   
B  F#  B    
znowu przegrywam trzy do dwoch   

F#  C#  C#m
Chociaz jak syreny 
G#m  F#              
budza mnie o tobie sny           
C#  C#m
Jedno wiem na pewno      
G#m
nie chcialbym bez Ciebie         

solo
F#  B 

F#
Niedogaszony pozar sie tli  
B  F#  B     
patrze i unosze brwi  
F#           
Za kare cisza, kara krzyk 
B  F#  B       
chcialem raz wygrac - pozwol mi  

F#
Kazesz cofnac sie o metr     
B  F#  B    
i nie wiem jak powiedziec nie
F#    
Wiec gryze warge, burze krew
B  F#  B     
trzaskasz drzwiami a ja klne     

solo
F#  C#  G#

C#       
Chociaz jak syreny
G#  F#                  
budza mnie o tobie sny   
C#      
Jedno wiem na pewno      
G#       
nie chcialbym bez Ciebie zyc    

F# B 

untill the end
```


## Avenged Sevenfold - So far away

[Back to Table of Contents](#user-content-table-of-contents)


```

 
[Verse 1]
Em
Never feared for anything
Em
Never shamed but never free
  G                D
A life to heal the broken heart
     Bm          C
With all that it could
 
Em
Lived the life so endlessly
Em
Saw beyond what others see
  G                  D
I tried to heal your broken heart
     Bm         C
With all that I could
 
[Bridge]
C
Will you stay
D
Will you stay away forever?
 
[Chorus]
Em                        G
How do I live without the ones I love?
Am                                Em/G       D
Time still turns the pages of the book, it's burned
Em                     G
Place and time, always on my mind
  Am                                C    D
I have so much to say but you're so far away
 
[Verse 2]
Em
Plans of what our futures hold
Em
Foolish lies of growing old
    G              D
It seems we're so invincible
    Bm          C
The truth is so cold
 
Em
A final song, a last request
Em
A perfect chapter laid to rest
G              D
Now and then I try to find
    Bm          C
The place in my mind
 
[Bridge]
C
Where you can stay
D
You can stay awake forever
 
[Chorus]
Em                        G
How do I live without the ones I love?
Am                                Em/G         D
Time still turns the pages of the book, it's burned
Em                    G
Place and time always on my mind
  Am                                C     D
I have so much to say but you're so far away
 
[Verse 3]
E
Sleep tight
     C
I'm not afraid (not afraid)
    Am                    Em/G         D
The ones that we love are here with me
E
Lay away
  C
A place for me (place for me)
          D
'Cause as soon as I'm done
           C
I'll be on my way
    D
To live eternally
 
[Solo]
E  C  Am  Em/G  D
E  C  D  C  D
 
[Chorus]
Em                        G
How do I live without the ones I love?
Am                                Em/G          D
Time still turns the pages of the book, it's burned
Em                    G
Place and time always on my mind
        Am
And the light you left remains
          C         D
But it so hard to stay
      Am                                C     D    Em
And I have so much to say and you're so far away
 
[Solo] 3:30
Em  D  Am  G  D/F#  Em  D  C x4
Em  D  C
 
[Outro]
Em
I love you
D
You were ready
Am         G       D/F#       Em    D    C
The pain is strong and urges rise
Em
But I'll see you
D
When He lets me
Am           G          D/F#     Em    D    C
Your pain is gone, your hands untied
 
Em D     C   Em         D             C
So far away      (And I need you to know)
Em D     C   Em         D                    C
So far away      (And I need you to need you to know)


```


## Birdy - Deep End

[Back to Table of Contents](#user-content-table-of-contents)


```

==================
 Birdy - Deep End
==================
 
[Intro]
Bm D G Bm
Bm D G Bm
 
[Verse 1]
Bm      D            G         Bm
Someone told me that I’ll want more
          Bm         D      G           Bm
That I'll feel half, empty, ripped, and torn
         Bm          D            G        Bm
They say there'll be plenty other hands to hold
      Bm          D       G     Bm
Now I wish they'd told me long ago
 
[Pre-Chorus]
Em D          G            D
Oooh, I don't want this to break you
Em D               G              D
Oooh, but I've got no one else to talk to
 
[Chorus]
        G           D                  Bm A
I don't know if you mean everything to me
      G           D                   Bm   F#m
And I wonder, can I give you what you need?
      G            D
Don't want to find I've lost it all
    A/C#           Bm
Too scared to have no one to call
Em        D       G
So can we just pretend
               Em        D        G
That we're not falling into the deep end?
 
[Verse 2]
Bm          D      G         Bm
You've gone quiet, you don't call
    Bm        D     G  Bm
And nothing's funny anymore
    Bm        D         G        Bm
And I'll keep trying to help you heal
     Bm       D          G        Bm
I'll stop you crying and dry your tears
 
[Pre-Chorus]
Em D          G            D
Oooh, I don't want this to break you
Em D               G              D
Oooh, but I've got no one else to talk to
 
[Chorus]
        G           D                  Bm A
I don't know if you mean everything to me
      G           D                   Bm   F#m
And I wonder, can I give you what you need?
      G            D
Don't want to find I've lost it all
    A/C#           Bm
Too scared to have no one to call
Em        D       G
So can we just pretend
 
[Bridge]
G    A   D      A  G    D
Oooh-ooh how do we mend?
G    A            D         A G    D  G A
Oooh-ooh I didn't choose to depend on you
     Bm     A   Em
It's out of our hands
      Em           D        G
Maybe it will work out in the end
 
[Chorus]
        G           D                  Bm A
I don't know if you mean everything to me
      G           D                   Bm   F#m
And I wonder, can I give you what you need?
      G            D
Don't want to find I've lost it all
    A              Bm
Too scared to have no one to call
Em        D       G
So can we just pretend
 
[Chorus]
        G           D                  Bm A
I don't know if you mean everything to me
      G           D                   Bm   F#m
And I wonder, can I give you what you need?
      G            D
Don't want to find I've lost it all
    A/C#           Bm
Too scared to have no one to call
Em        D       G
So can we just pretend
               Em        D        G
That we're not falling into the deep end?

```


## Coldplay - Fix you DE

[Back to Table of Contents](#user-content-table-of-contents)


```

Wann du tust dein Bestes, aber es nicht gelingt 					E, Emajor7, c#m7, B
Wann du kriegst was du willst und nicht was du brauchst
Wann du bist so m�de aber kannst nicht schlafen
Steckenbleiben im R�ckw�rtsgang

Wann die Tr�nen str�men �bers Gesicht
Wann du verlierst etwas, was du ersetzt nicht 
Wann du liebst jemanden, aber es wird vergeudet
Konnte es schlechter sein?

Die Lichter f�hren dich nach Hause									A B
und deine Knochen lodern
und ich versuche, dich �berzuholen

Hoch �ber oder weit unten
Wann du liebst zu stark es aufzugeben
Aber versuchst du nie, wirst du nie wissen
Nur was du wert bist

Die Lichter f�hren dich nach Hause									A	B
und deine Knochen lodern
und ich versuche, dich �berzuholen

Die Tr�nen str�men �bers Gesicht									E 	A
Wann du verlierst etwas du ersetzt nicht						E 	Emajor7 	B
Die Tr�nen str�men �bers Gesicht 								c#m7	A	
und ich																		E	B

Die Tr�nen str�men �bers Gesicht 								E	A
Und ich verspreche dir, ich lerne von meinen F�hlern		E 	Emajor7 	B
Die Tr�nen str�men �bers Gesicht 								c#m7	A
und ich																		E	B

Die Lichter f�hren dich nach Hause
und deine Knochen lodern
und ich versuche, dich �berzuholen
```


## Coldplay - Fix you

[Back to Table of Contents](#user-content-table-of-contents)


```

When you try your best but you don't succeed 
When you get what you want but not what you need 
When you feel so tired but you can't sleep 
Stuck in reverse 

When the tears come streaming down your face 
When you lose something you can't replace 
When you love someone but it goes to waste 
Could it be worse? 

Lights will guide you home 
And ignite your bones 
And I will try to fix you 

High up above or down below 
When you' re too in love to let it go 
But if you never try you'll never know 
Just what you're worth 

Lights will guide you home 
And ignite your bones 
And I will try to fix you 

Tears stream down your face 
When you lose something you cannot replace 
Tears stream down your face and I 

Tears stream down your face 
I promise you I will learn from my mistakes 
Tears stream down your face and I 

Lights will guide you home 
And ignite your bones 
And I will try to fix you
```


## Coldplay - Hymn for the weekend

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]

Dm G Am Am


[Verse]

                Dm       G    Am
Ohhhhh, angels sent from up above
              Dm        G        Am
You know you make my world light up
                Dm
When I was down
     G       Am
When I was hurt
             Dm      Am
You came to lift me up...

            Dm         G        Am
Life is a drink, and love's a drug
           Dm             G     Am
Oh now I think I must be miles up
             Dm       G          Am
When I was hurt, withered, dried up
             Dm       Am
You came to rain a flood


[Pre-Chorus]

              Am             F
So drink from me, drink from me
            Dm
When I was so thirsty
             Am
Pour on a symphony
     G
Now I just can't get enough

                Am             F
Put your wings on me, wings on me
           G
When I was so heavy
          Am
Pour on a symphony
        C
When I'm lower, lower, lower, low


[Chorus]

 F    G   Am
I-Oh-ah-oh-ah
                        F
I'm feeling drunk and high
     G       Am
So high, so high
 F    G   Am
I-oh-ah-oh-ah
                        F
I'm feeling drunk and high
     G        Am
So high, so high


[Instrumental]

Dm G Am
Dm G Am


[Verse]

                Dm       G    Am
Ohhhhh, angels sent from up above
              Dm        G        Am
I feel you coursing through my blood
            Dm         G        Am
Life is a drink, your love's about
                Dm        Am
To make the stars come out


[Pre-Chorus]

                Am             F
Put your wings on me, wings on me
           G
When I was so heavy
          Am
Pour on a symphony
        C
When I'm lower, lower, lower, low


[Chorus]

 F    G   Am
I-Oh-ah-oh-ah
                        F
I'm feeling drunk and high
     G       Am
So high, so high
 F    G   Am
I-oh-ah-oh-ah
                        F
I'm feeling drunk and high
     G        Am
So high, so high

 F    Dm   Am
I-oh-ah-oh-ah
              F         G       Am
la-la-la-la-la-la high so high so high

 F    G   Am
I-oh-ah-oh-ah
                        F
I'm feeling drunk and high
     G        Am
So high, so high


[Outro]

                                F G Am
Then we'll shoot across the sky
                           F G Am
Then we'll shoot across the ...
                           F G Am
Then we'll shoot across the sky
                           F G Am
Then we'll shoot across the ...

                               F G Am
Then we'll shoot across the sky
                               F G Am
Then we'll shoot across the...
                            N.C.
Then we'll shoot across the sky
N.C.                             
Then we'll shoot across the...
```


## Coldplay - Violet hill

[Back to Table of Contents](#user-content-table-of-contents)


```

[Verse 1]

       C#m
Was a long and dark December
                  C#sus2
From the rooftops I remember
          A   Ama7        F#m   F#m6
There was snow ,           white snow
  C#m
Clearly I remember from the windows
          C#sus2                A   Ama7
They were watching while we froze
        F#m   F#m6
Down below


[Chorus]

          A
When the future's architectured
     B                     C#m
By a carnival of idiots on show
                 B
You'd better lie low
F#(note) A    G#m E        C#m  B  C#m
If you  love me, won�t you let me know?


[Verse 2]

      C#m
Was a long and dark December
                      C#sus2
When the banks became cathedrals
        A    Ama7         F#m    F#m6
And a fox                 became God
 C#m
Priests clutched onto bibles
                         C#sus2
Hollowed out to fit their rifles
         A    Ama7            F#m     F#m6
And the cross              was held aloft


[Chorus]

A
Bury me an armour
         B
When I�m dead and hit the ground
              C#m           B
My nerves are poles that unfroze
F#(note) A    G#m E        C#m  B  C#m
If you  love me, won�t you let me know?


[Solo]

C#m C#m A F#m  x2

(don't forget about the capo thing...it's 2 step higher so if you play without a capo, 3
a 5, 2 is a 7, etc...)

(3x)                        (1x)
e|--------------------------|-----------------------------|
B|-3p2p0-3------------------|--3p2p0-3--------------------|  (2x)
G|------------2-----4-------|-------------2---------------|
D|---------4-----4-----2----|----------4-----4--9-8--7-(6)|
A|-----------------------2--|-------------------7-6--5-(4)|
E|--------------------------|-------------------7-6--5-(4)|

(the last chord is played the second time only, and the first
time, you just return to beginning of the solo...)


[Chorus]

        A
I don�t want to be a soldier
           B
Who the captain of some sinking ship
      C#m
Would stow
    B
Far below
F#(note)   A    G#m E         C#m  B  C#m
So, if you love me, why�d you let me go?


[Outro]

          E    A       E  C#m  B    A
I took my love down to Violet Hill
B        C#m     G#m     A
There we sat    in the  snow
A   B    C#m   Em(2)    E  C#m  B
All that time she was silent still
    G#m     A    G#m E         C#m  B    A
So, if you love me, won�t you let  me   know?
G#m     A   G#m E         C#m  B  C#m
If you love me, won�t you let me know?

```


## Dua Lipa - Levitating

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]
Dm Am Gm Dm x2
 
 
[Verse 1]
Dm                         Am
If you wanna run away with me, I know a galaxy
    Gm                     Dm
And I could take you for a ride
  Dm                        Am
I had a premonition that we fell into a rhythm
          Gm                   Dm
Where the music don't stop for life
Dm                  Am
Glitter in the sky, glitter in my eyes
Gm                       Dm
Shining just the way you like
Dm                                Am
If you're feeling like you need a little bit of company
    Gm                    Dm
You met me at the perfect time
 
 
[Pre-Chorus]
Dm            Am
  You want me, I want you, baby
Gm            Dm
  My sugar-boo, I'm levitating
Dm              Am
  The Milky Way, we're renegading
Gm                      Dm
Yeah, yeah, yeah, yeah, yeah
 
 
[Chorus]
      Dm   Am          Gm        Dm
I got you, moonlight, you're my starlight
       Dm  Am          Gm                    Dm
I need you all night, come on dance with me
 
I'm levitating
      Dm   Am          Gm        Dm
I got you, moonlight, you're my starlight
       Dm  Am          Gm                    Dm
I need you all night, come on dance with me
 
I'm levitating
 
 
Dm Am Gm Dm
 
 
[Verse 2]
Dm                              Am
I believe that you're for me, I feel it in our energy
  Gm                    Dm
I see us written in the stars
Dm                           Am
We can go wherever, so let's do it now or never
      Gm                      Dm
Baby, nothing's ever ever too far
Dm                  Am
Glitter in the sky, glitter in our eyes
Gm                      Dm
Shining just the way we are
  Dm                            Am
I feel like we're forever every time we get together
    Gm                          Dm
But whatever, let's get lost on Mars
 
 
[Pre-Chorus]
Dm            Am
  You want me, I want you, baby
Gm            Dm
  My sugar-boo, I'm levitating
Dm              Am
  The Milky Way, we're renegading
Gm                      Dm
Yeah, yeah, yeah, yeah, yeah
 
 
[Chorus]
      Dm   Am          Gm        Dm
I got you, moonlight, you're my starlight
       Dm  Am          Gm                    Dm
I need you all night, come on dance with me
 
I'm levitating
      Dm   Am          Gm        Dm
I got you, moonlight, you're my starlight
       Dm  Am          Gm                    Dm
I need you all night, come on dance with me
 
I'm levitating
 
 
[Post-Chorus]
Dm              Am                Gm
  You could fly away with me tonight
               Dm
You could fly away with me tonight
Dm             Am
  Baby, let me take you for a ride
Gm                      Dm
Yeah, yeah, yeah, yeah, yeah
 
I'm levitating
Dm              Am                Gm
  You could fly away with me tonight
               Dm
You could fly away with me tonight
Dm             Am                  Gm
  Baby, let me take you for a ride
                        Dm
Yeah, yeah, yeah, yeah, yeah
 
I'm levitating
 
 
[Bridge]
Dm                                 Am
My love is like a rocket, watch it blast off
        Gm                             Dm
And I'm feeling so electric, dance our ass off
Dm                         Am
And even if I wanted to, I can't stop
Gm                      Dm
Yeah, yeah, yeah, yeah, yeah
Dm                                 Am
My love is like a rocket, watch it blast off
        Gm                             Dm
And I'm feeling so electric, dance our ass off
Dm                         Am
And even if I wanted to, I can't stop
Gm                      Dm
Yeah, yeah, yeah, yeah, yeah
 
 
[Pre-Chorus]
Dm            Am
  You want me, I want you, baby
Gm            Dm
  My sugar-boo, I'm levitating
Dm              Am
  The Milky Way, we're renegading
 
 
[Chorus]
      Dm   Am          Gm        Dm
I got you, moonlight, you're my starlight
       Dm  Am          Gm                    Dm
I need you all night, come on dance with me
 
I'm levitating
 
 
[Post-Chorus]
Dm              Am                Gm
  You could fly away with me tonight
               Dm
You could fly away with me tonight
Dm             Am                  Gm
  Baby, let me take you for a ride
                        Dm
Yeah, yeah, yeah, yeah, yeah (I'm levitating)
Dm              Am                Gm
  You could fly away with me tonight
               Dm
You could fly away with me tonight
Dm             Am                  Gm
  Baby, let me take you for a ride
 
 
[Chorus]
      Dm   Am          Gm        Dm
I got you, moonlight, you're my starlight
       Dm  Am          Gm                    Dm
I need you all night, come on dance with me
 
I'm levitating
      Dm   Am          Gm        Dm
I got you, moonlight, you're my starlight
       Dm  Am          Gm                    Dm
I need you all night, come on dance with me
 
I'm levitating
X
By helping UG you make the world better... and earn IQ

```


## Dżem - Partyzant

[Back to Table of Contents](#user-content-table-of-contents)


```

„PARTYZANT” - DZEM
Kryję twarz za zasłoną rąk…. a C
Gdybym mógł pewnie uciekłbym stąd…. G D/F#
Ciągle myślę: Gdzie był błąd?…. a C
Gdzie był mój błąd.... E7
Wkoło grzmią kanonady bomb…. a C
A mój dom parę kroków stąd…. G D/F#
Paru ludzi jeszcze wczoraj a dziś…. a C
Zostałem całkiem sam…. E7
Czy los…. D
Przesądzony jest czy zgubiony jestem już?.... C D
Mój los zły los…. D C
Bronię tego miejsca jak lew…. a C
Za tych co przelali tu krew…. G D/F#
Jeszcze dzień może wytrwam tu dwa…. a C
Może dwa.... E7
Boże mój czemu bawią się tak? …. a C
W imię czego niszczą mój świat? …. G D/F#
I nadzieję na następny dzień…. a C
I wiarę... I wiarę w sens: .... E7
(REF:)
Tak walczę ze złem…. a
A zło rośnie we mnie z każdym dniem …. b E
Jaki w tym sens?.... a D
Gdy zabijam…. E7
Czuję się lżej…. a
I coraz mnie mniej…. D
A zło rośnie we mnie z każdym dniem…. b E
Jaki w tym sens?.... a D
Jaki sens... .E7
D… C…. D…. C
Tu niedawno stał mój dom …. a C
Wszystko kwitło po horyzont …. G D/F#
A spokoju nie zakłócał nikt…. a C
Nie zakłócał nikt... .E7
Odkąd ktoś kilometry w głąb…. a C
Znalazł ropę kazał zwijać się stąd …. G D/F#
Wziąłem broń broniąc tego co mam …. a C
Stałem się złym partyzantem, którego ściga prawy świat... .E7
(REF:) …….. Czy los…. D…….. (REF:)
```


## Frank Sinatra - Fly Me to the Moon

[Back to Table of Contents](#user-content-table-of-contents)


```

Fly Me To The Moon chords
Frank Sinatra

Am            Dm7          G7             Cmaj7 
Fly me to the moon, let me play among the stars,  
F               Dm                E7          Am  A7
Let me see what spring is like on Jupiter and mars, 
 
   Dm7       G7  C       Am
In other words,  hold my hand!  
   Dm7       G7 C         E   
In other words, baby kiss me! 
 
Am                 Dm7              G7           Cmaj7 
Fill my heart with song, and let me sing forever more  
F             Dm             E          Am   A7
you are all I long for all I worship & adore 
 
   Dm7       G7  C         Am   E7
In other words,  please be true! 
   Dm7       G7 Fm     C       
In other words  I love you 

Am Dm7 G7 Cmaj7 F Dm E Am   Dm7 G7 Em A7 E7   Dm7 G7 C

Am                 Dm7              G7           Cmaj7 
Fill my heart with song, and let me sing forever more  
F             Dm             E          Am   A7
you are all I long for all I worship & adore 
 
   Dm7       G7  C         Am
In other words,  please be true! 
   Dm7       G7  
In other words 
   Dm7          G G7   C
In other words  I love you! 
```


## Frank Sinatra - The lady is a tramp

[Back to Table of Contents](#user-content-table-of-contents)


```

[Verse]

C            Cdim         Dm7        G7
She gets too hungry for dinner at eight;
C             Cdim          Dm7          G7
She adores the theater, and won't arrive late
C          Cdim         C9           F  Fm
She never bothers with people she hates,
C      C/B      F   G7     C   Am  G#7  G7
That's why the lady is a tramp.

     C          Cdim             Dm7         G7
She don't go to crap games with Barons and Earls,
C            Cdim       Dm7       G7
Won't go to Harlem in ermine and pearls,
C              Cdim        C9          F  Fm
Won't dish the dirt like some of the girls
C      C/B      F   G7      C    C7
That's why the lady is a tramp.


[Chorus]

               Fm7   G7    Em7         Am
She likes the free, fresh wind in her hair
Dm7            G   C         A7   D7      G7
Life without care;   she's broke,   it's oke.
C           Cdim             Dm7            E7
Hates Cal-i-for - nia, it's cold and it's damp
Am     Am7     D7   G7     C     Am   D7   G7
That's why the lady is a tramp.
```


## Green Day - Buolevard of broken dreams

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]
 
Em  G  D  A
Em  G  D  A
 
 
[Verse 1]
 
Em        G                D             A              Em
 I walk a lonely road, the only one that I have ever known
           G              D                A               Em
Don't know where it goes, but it's home to me and I walk alone
 
 
[Interlude]
 
(Em) G  D  A
 
 
[Verse 2]
 
Em           G             D            A              Em
 I walk this empty street, on the boulevard of broken dreams
          G                D             A               Em
Where the city sleeps, and I'm the only one and I walk alone
 
 
[Interlude]
 
(Em) G  D           A             Em
           I walk alone, I walk alone
(Em) G  D           A
           I walk alone, I walk a....
 
 
[Chorus]
 
C       G            D              Em
    My shadow's the only one that walks beside me
C        G       D               Em
    My shallow heart's the only thing that's beating
C        G       D                 Em
    Sometimes I wish someone out there will find me
C         G      B7
    Till then I walk alone
 
 
[Interlude]
 
 Em    G     D       A
Ah-Ah Ah-Ah Ah-Ah   Ahhh-Ah
     Em   G     D       A
haaa-ah  Ah-Ah Ah-Ah   Ah-Ah
 
 
[Verse 3]
 
Em           G
 I'm walking down the line
D               A               Em
That divides me somewhere in my mind
       G           D
On the border line of the edge
    A              Em
And where I walk alone
 
 
[Interlude]
 
(Em) G  D  A
 
 
[Verse 4]
 
Em      G
 Read between the lines
D                    A              Em
What's fucked up and everything's alright
         G               D               A
Check my vital signs, to know I'm still alive
             Em
And I walk alone
 
 
[Interlude]
 
(Em) G  D           A             Em
           I walk alone, I walk alone
(Em) G  D           A
           I walk alone, I walk a....
 
 
[Chorus]
 
C       G            D              Em
    My shadow's the only one that walks beside me
C        G       D               Em
    My shallow heart's the only thing that's beating
C        G       D                 Em
    Sometimes I wish someone out there will find me
C         G      B7
    Till then I walk alone
 
 
[Interlude]
 
 Em    G     D    A
Ah-Ah Ah-Ah Ah-Ah   Ahhh-Ah
     Em   G     D       A
haaa-ah  Ah-Ah Ah-Ah  I walk alone, I walk a...
 
 
[Solo]
 
C  G  D  Em
C  G  D  Em
C  G  D  Em
C  G  B  B7
 
 
[Verse 5]
 
Em          G             D           A               Em
I walk this empty street, on the boulevard of broken dreams
           G               D              A
Where the city sleeps, and I'm the only one and I walk a...
 
 
[Chorus]
 
C       G            D              Em
    My shadow's the only one that walks beside me
C        G       D               Em
    My shallow heart's the only thing that's beating
C        G       D                 Em
    Sometimes I wish someone out there will find me
C         G      B
    Till then I walk alone
 
 
[Outro]
 
Em  C  D   A/C#  G  D#5
Em  C  D   A/C#  G  D#5
Em  C  D   A/C#  G  D#5
Em  C  D   A/C#  G  D#5


```


## Grzegorz Turnau - Naprawdę nie dzieje się nic

[Back to Table of Contents](#user-content-table-of-contents)


```


tonacja d-moll

[Verse 1]
    Am
Czy zdanie okrągłe wypowiesz
    Cm             Gm
Czy księgę mądrą napiszesz
         Dm
Będziesz zawsze mieć w głowie
   Cm            Gm
Tę samą pustkę i ciszę

Am
Słowo to zimny powiew
  Cm                 Gm
Nagłego wiatru w przestworze
      Dm
Może orzeźwi cię
      Cm                Gm
Ale donikąd dojść nie pomoże

Dm Am Dm Am Dm


[Chorus]
Dm         C    F        C       F    A# F A#
Zwieść cię może ciągnący ulicami tłum
Dm      C       F      A#         A      A7
Wódka w parku wypita albo zachód słońca
       Gm        Dm         Am         Cm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Gm         Am        Dm
I nie stanie się nic aż do końca
       Gm        Dm         Am         Cm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Gm         Am        Dm    Gm
I nie stanie się nic aż do końca

Gm Dm Gm Dm Gm Am


[Verse 2]
    Dm
Czy zdanie okrągłe wypowiesz
    Cm             Gm
Czy księgę mądrą napiszesz
         Dm
Będziesz zawsze mieć w głowie
   Cm            Gm
Tę samą pustkę i ciszę

  Dm
Zaufaj tylko warg splotom
   Cm            Gm
Bełkotom niezrozumiałym
         Dm
Gestom w próżni zawisłym
Cm          Gm
    Niedoskonałym

Dm Am Dm Am Dm


[Chorus]
Dm         C    F        C       F    A# F A#
Zwieść cię może ciągnący ulicami tłum
Dm      C       F      A#         A      A7
Wódka w parku wypita albo zachód słońca
       Gm        Dm         Am         Cm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Gm         Am        Dm
I nie stanie się nic aż do końca
       Gm        Dm         Am         Cm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Gm         Am        Dm    Gm
I nie stanie się nic aż do końca

Gm Dm Gm Dm Gm Am


Dm         C    F        C       F    A# F A#
Zwieść cię może ciągnący ulicami tłum
Dm      C       F      A#         A      A7
Wódka w parku wypita albo zachód słońca
       Gm        Dm         Am         Cm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Gm         Am        Dm
I nie stanie się nic aż do końca
       Gm        Dm         Am         Cm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Gm         Am        Dm    Gm
I nie stanie się nic aż do końca

Gm Dm Gm Dm Gm Am

What's this?




[Verse 1]
    Em
Czy zdanie okrągłe wypowiesz
    Gm             Dm
Czy księgę mądrą napiszesz
         Am
Będziesz zawsze mieć w głowie
   Gm            Dm
Tę samą pustkę i ciszę

Em
Słowo to zimny powiew
  Gm                 Dm
Nagłego wiatru w przestworze
      Am
Może orzeźwi cię
      Gm                Dm
Ale donikąd dojść nie pomoże

Am Em Am Em Am


[Chorus]
Am         G    C        G       C    F C F
Zwieść cię może ciągnący ulicami tłum
Am      G       C      F         E      E7
Wódka w parku wypita albo zachód słońca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am
I nie stanie się nic aż do końca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am    Dm
I nie stanie się nic aż do końca

Dm Am Dm Am Dm Em


[Verse 2]
    Am
Czy zdanie okrągłe wypowiesz
    Gm             Dm
Czy księgę mądrą napiszesz
         Am
Będziesz zawsze mieć w głowie
   Gm            Dm
Tę samą pustkę i ciszę

  Am
Zaufaj tylko warg splotom
   Gm            Dm
Bełkotom niezrozumiałym
         Am
Gestom w próżni zawisłym
Gm          Dm
    Niedoskonałym

Am Em Am Em Am


[Chorus]
Am         G    C        G       C    F C F
Zwieść cię może ciągnący ulicami tłum
Am      G       C      F         E      E7
Wódka w parku wypita albo zachód słońca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am
I nie stanie się nic aż do końca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am    Dm
I nie stanie się nic aż do końca

Dm Am Dm Am Dm Em


Am         G    C        G       C    F C F
Zwieść cię może ciągnący ulicami tłum
Am      G       C      F         E      E7
Wódka w parku wypita albo zachód słońca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am
I nie stanie się nic aż do końca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am    Dm
I nie stanie się nic aż do końca

Dm Am Dm Am Dm Em
[Verse 1]
    Em
Czy zdanie okrągłe wypowiesz
    Gm             Dm
Czy księgę mądrą napiszesz
         Am
Będziesz zawsze mieć w głowie
   Gm            Dm
Tę samą pustkę i ciszę

Em
Słowo to zimny powiew
  Gm                 Dm
Nagłego wiatru w przestworze
      Am
Może orzeźwi cię
      Gm                Dm
Ale donikąd dojść nie pomoże

Am Em Am Em Am


[Chorus]
Am         G    C        G       C    F C F
Zwieść cię może ciągnący ulicami tłum
Am      G       C      F         E      E7
Wódka w parku wypita albo zachód słońca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am
I nie stanie się nic aż do końca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am    Dm
I nie stanie się nic aż do końca

Dm Am Dm Am Dm Em


[Verse 2]
    Am
Czy zdanie okrągłe wypowiesz
    Gm             Dm
Czy księgę mądrą napiszesz
         Am
Będziesz zawsze mieć w głowie
   Gm            Dm
Tę samą pustkę i ciszę

  Am
Zaufaj tylko warg splotom
   Gm            Dm
Bełkotom niezrozumiałym
         Am
Gestom w próżni zawisłym
Gm          Dm
    Niedoskonałym

Am Em Am Em Am


[Chorus]
Am         G    C        G       C    F C F
Zwieść cię może ciągnący ulicami tłum
Am      G       C      F         E      E7
Wódka w parku wypita albo zachód słońca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am
I nie stanie się nic aż do końca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am    Dm
I nie stanie się nic aż do końca

Dm Am Dm Am Dm Em


Am         G    C        G       C    F C F
Zwieść cię może ciągnący ulicami tłum
Am      G       C      F         E      E7
Wódka w parku wypita albo zachód słońca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am
I nie stanie się nic aż do końca
       Dm        Am         Em         Gm
Lecz pamiętaj, naprawdę nie dzieje się nic
      Dm         Em        Am    Dm
I nie stanie się nic aż do końca

Dm Am Dm Am Dm Em

```


## Happysad - Bez znieczulenia

[Back to Table of Contents](#user-content-table-of-contents)


```

Hml   C#ml            F#ml     E  
Hej ja przed tobą się rozbieram 
Hml   C#ml            A    C#ml
Zrzucam zmięte brudne....myśli
Hml   C#ml            F#ml     E  
I przed tobą umieram 
Hml   C#ml            A
Chore serce otwieram 
E
Bez znieczulenia

Przejście
Dsus C#ml A E

Hej ja przed tobą się rozklejam
Wcieraj ciepłe lepkie wspomnienia
Tylko w tobie nadzieja
Teraz chore serce otwieraj
Bez znieczulenia

Przejście
Dsus C#ml A E

          D
Bo w mojej głowie zamieć
F#ml
Czarne czarne chmury
D                    F#ml
Zawierucha deszcz do spółki z gradem
        D                    F#ml
Jakieś obce znów głosy podpowiadają mi bzdury
    D                                      F#ml
Tak trudno mi trudno mi trudno mi trudno mi być

Hml   C#ml            F#ml     E  
Hej ja przed tobą umieram
Hml   C#ml            A     C#ml
Masuj obolałe niedomówienia
Hml   C#ml            F#ml     E  
Tylko w Tobie nadzieja
Hml   C#ml            A 
Teraz chore serce otwieraj
E
Bez znieczulenia

Przejście
Dsus C#ml A E

D
Bo w mojej głowie zamieć
F#ml
Czarne, czarne chmury
Zawierucha, deszcz do spółki z gradem
Jakieś obce znów głosy podpowiadają mi bzdury
Tak trudno mi, trudno mi, trudno mi, trudno mi być
Z Tobą razem

[Solo - zagraj przejście]
Dsus C#ml A E

Bo w mojej głowie zamieć
Czarne, czarne chmury
Zawierucha deszcz do spółki z gradem
Jakieś obce znów głosy podpowiadają mi bzdury
Tak trudno mi, trudno mi, trudno mi, trudno mi być
Z Tobą razem

```


## Happysad - Mów mi dobrze

[Back to Table of Contents](#user-content-table-of-contents)


```

[Verse 1]
Am                         Dm
  Mów mi dobrze, dobrze mi mów
             G                    Am
Łaskocz czule warkoczem ciepłych słów
                          Dm
Wilgotnym szeptem przytul mnie
                G           Am
I mów mi dobrze, nie mów mi źle

[Interlude]
Am Dm G Am 

[Verse 2]
Am                                   Dm
  Rozłóż przede mną księgę zaklęć i wróżb
            G                    Am
Okutą w obwolutę twoich ramion i ud
                                 Dm
I czaruj we mnie na ołtarz duszę złóż
                 G                   Am
I w dusznym kącie rzuć na mnie urok swój

[Verse 3]
Am                                 Dm
  I niech utonę w gorących wodach mórz
                  G                   Am
Cichych westchnień wyssanych z twoich ust
                                 Dm
I mów mi dobrze językiem jakim chcesz
             G                  Am
Mów mi dobrze, tylko nie mów mi źle

[Interlude]
Am Dm G Am 

Am Dm  x2

[Bridge]
Am                                   Dm
  Bo ja na złe, bo ja na złe reaguję źle
           Dm
I kurczę się, w sobie zamykam
           Dm
I kurczę się, w sobie zamykam

Am                                   Dm
  Bo ja na złe, bo ja na złe reaguję źle
           Dm
I kurczę się, w sobie zamykam
           Dm
I kurczę się, w sobie zamykam

Am                     Dm
  Bo ja na złe reaguję źle
                G
I kurczę się, w sobie zamykam
Am                     Dm
  Bo ja na złe reaguję źle
                G
I kurczę się, w sobie zamykam
 
[Interlude]
Am Dm G Am 

[Outro]

Am                         Dm
  Mów mi dobrze, dobrze mi mów
                G          Am
I mów mi dobrze, dobrze mi mów
                          Dm
Mów mi dobrze, mów dobrze mi
             G              Am
Mów mi dobrze, albo nie mów nic



```


## Hillsong United Oceans

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]

Bm   A/C#    D      A      G


[Verse 1]

Bm              A/C#       D                       A                       G
You call me out upon the waters         The great unknown where feet may fail
Bm                  A/C#     D                     A                    G
And there I find You in the mystery    In oceans deep   My faith will stand


[Chorus]

G           D              A     G            D             A                    G
And I will call upon Your name   And keep my eyes above the waves   When oceans rise    
              D              A                G         (A)     Bm
My soul will rest in your embrace  For I am Yours  and You are mine


[Interlude]

Bm A/C#  D  A  Em/G


[Verse 2]

Bm                     A/C#    D                       A                  G
Your grace abounds in deepest waters   Your sovereign hand   Will be my guide
Bm                      A/C#      D                       A                       G
Where feet may fail and fear surrounds me   You've never failed and you won't start now

Chorus:
G           D              A     G            D             A                    G
So I will call upon Your name   And keep my eyes above the waves   When oceans rise    
              D              A                G         (A)     Bm
My soul will rest in your embrace  For I am Yours  and You are mine


[Instrumental]
  
Bm A/C#  D  A  Em/G                
Bm A/C#  D  A  Em/G 
               
Bm  G  D  A Bm  G  D  A


[Bridge]

Bm                        G
Spirit lead me where my trust is without borders     
        D                           A
Let me walk upon the waters  Wherever You would call me
Bm                      G
Take me deeper than my feet could ever wander
         D                                   A
And my faith will be made stronger   In the presence of my Saviour      x3

G                         D
Spirit lead me where my trust is without borders     
         A                          Em
Let me walk upon the waters  Wherever You would call me
G                       D
Take me deeper than my feet could ever wander
          A                                   Em
And my faith will be made stronger   In the presence of my Saviour

Bm              A/C#      D
Spirit lead me where my trust is without borders     
        A                          Em
Let me walk upon the waters  Wherever You would call me
Bm              A/C#     D
Take me deeper than my feet could ever wander
         A                                   Em
And my faith will be made stronger   In the presence of my Saviour            x2


[Instrumental]

Bm  A/C#  D  A  Em                
Bm  A/C#  D  A  Em   


[Chorus]

G       D              A 
I will call upon Your Name    
G        D              A    
Keep my eyes above the waves
G            D              A 
My soul will rest in Your embrace
       G       A      Bm  A/C#  D  A  G  Bm  A/C#  D  
I am Yours and You are mine
```


## If I Stay - Heart Like Yours - Willamette Stone

[Back to Table of Contents](#user-content-table-of-contents)


```

[Verse 1]

G                     Em
Breathe Deep, Breathe Clear
              C                        Am
know that I'm here, know that I'm here

waiting
G                 Em
Stay strong, stay gold
                  C                            Am
you don't have to fear, you don't have to fear

waiting


[Pre-Chorus]

Em               C                 Am
I, I'll see you soon, I'll see you soon


[Chorus]

            G               
How could a heart like yours
Am           Em             
ever love a heart like mine
C            G           
how could I live before
Am               Em           
How could I have been so blind
    C          G
You opened up my eyes
    Am        Em      
You opened up my eyes


[Verse 2]

G                  Em
Sleep sound, sleep tight
            C                   Am
Here in my mind, here in my mind

waiting
G             Em
Come close my dear
                  C                            Am
you don't have to fear, you don't have to fear

waiting


[Pre-Chorus]

Em               C                 Am
I, I'll see you soon, I'll see you soon


[Chorus]
            G               
How could a heart like yours
Am           Em             
ever love a heart like mine
C            G           
how could I live before
Am               Em           
How could I have been so blind
    C          G
You opened up my eyes
    Am        Em      
You opened up my eyes


[Bridge]

C        D
Holdfast hope
         Em               
all your love is all I've ever known
C        D
Holdfast hope 
         Em              
all your love is all I've ever known


[Chorus]

            G               
How could a heart like yours
Am           Em             
ever love a heart like mine
C            G           
how could I live before
Am               Em           
How could I have been so blind
    C          G
You opened up my eyes
    Am        Em      
You opened up my eyes

G
```


## I hear your voice - Why are you here

[Back to Table of Contents](#user-content-table-of-contents)


```

(Capo 1)

Intro
C, F
Dm, G, C

Verse
C, F
Dm, G, C
(x2)

Chorus
(C), F, E
Dm, G, C
A, F, E
Dm, G, C

Verse
Chorus (x2)

wae ijeya watni eodie isseotdeon geoni
jogeumeun neujeun deut ijeya mannatne

neon sarangeul midni
ireonge unmyeongin geoni
dal darhan maldeuri naege deulline

nae ape seo itneun niga utgo
ni yeopeseo ni soneul kkog jabgo
sarangi neomchineun nunbicheuro baraboda
yunanhi gabyeoun balgeoreume
jeojeollo naoneun i noraega
neol dalma geureolkka areumdaun i mellodi

wae useumi nalkka
jakkuman jeonhwagil bolkka
juwieseon naega jom byeonhan geot gatdae

na wae ireon geoni
jakkuman ni saenggagman na
kkum kkudeon sarangi neoyeo sseot nabwa

nae ape seo itneun niga utgo
ni yeopeseo ni soneul kkog jabgo
sarangi neomchineun nunbicheuro baraboda
yunanhi gabyeoun balgeoreume
jeojeollo naoneun i noraega
neol dalma geureolkka areumdaun i mellodi

ni ape seo itneun naega utgo
nae yeopeseo nae soneul kkog jabgo
sarangi neomchineun nunbicheuro
nareul boneun niga joha
yunanhi gabyeoun balgeoreume
jeojeollo naoneun i noraega
neol dalma geureolkka areumdaun i mellodi

wae ijeya watni
```


## Iron Maiden - Fear of the dark

[Back to Table of Contents](#user-content-table-of-contents)


```

[Verse 1]
 
Dm       Bb            C
  I am a man who walks alone,
             Bb      C      Dm
and when I'm walking a dark road
             Bb                   C         Bb      C      Dm
at night or strolling through the park
Dm       Bb              C
When the lights begin to change,
             Bb     C      Dm
I sometimes feel a little strange
         Bb                C
a little anxious when it's dark
 
 
[Chorus]
 
            Bb    C                Dm      C
Fear of the dark,      Fear of the dark,
         Bb                 F           C      Dm
I have a constant fear that something's always near
            Bb    C                Dm     C
Fear of the dark,      Fear of the dark,
         Bb          F         C      Dm
I have a phobia that someone's always there
 
 
[Verse 2]
 
Dm                   Bb              C
  Have you run your fingers down the wall
              Bb          C        Dm
And have you felt your neck skin crawl
             Bb                C       Bb      C      Dm
When you're searching for the light
          Dm           Bb               C
Sometimes when you're scared to take a look
        Bb        C   Dm
At the corner of the room
                    Bb                   C
You've sensed that something's watching you
 
 
[Chorus]
 
            Bb    C                Dm      C
Fear of the dark,      Fear of the dark,
         Bb                 F           C      Dm
I have a constant fear that something's always near
            Bb    C                Dm     C
Fear of the dark,      Fear of the dark,
         Bb          F         C      Dm
I have a phobia that someone's always there
 
 
[Verse 3]
 
Dm               Bb            C
  Have you ever been alone at night
             Bb        C      Dm
Thought you heard footsteps behind
             Bb                 C       Bb      C      Dm
And turned around and no one's there?
            Dm         Bb   C
And as you quicken up your pace
             Bb      C     Dm
You find it hard to look again
                Bb                    C
Because you're sure there's someone there
 
 
[Chorus]
 
            Bb    C                Dm      C
Fear of the dark,      Fear of the dark,
         Bb                 F           C      Dm
I have a constant fear that something's always near
            Bb    C                Dm     C
Fear of the dark,      Fear of the dark,
         Bb          F         C      Dm
I have a phobia that someone's always there
 
 
[Verse 4]
 
Dm                 Bb                C
  Watching horror films the night before
             Bb      C   Dm
Debating witches and folklore
             Bb               C       Bb      C      Dm
The unknown troubles on your mind
            Dm       Bb      C
Maybe your mind is playing tricks
                Bb      C    Dm
You sense, and suddenly eyes fix
             Bb           C
On dancing shadows from behind
 
 
[Chorus]
 
            Bb    C                Dm      C
Fear of the dark,      Fear of the dark,
         Bb                 F           C      Dm
I have a constant fear that something's always near
            Bb    C                Dm     C
Fear of the dark,      Fear of the dark,
         Bb          F         C      Dm
I have a phobia that someone's always there
 
            Bb    C                Dm      C
Fear of the dark,      Fear of the dark,
         Bb                 F           C      Dm
I have a constant fear that something's always near
            Bb    C                Dm     C
Fear of the dark,      Fear of the dark,
         Bb          F         C      Dm
I have a phobia that someone's always there
 
 
[Outro]
 
Dm           Bb             C
   When I'm walking a dark road
          Bb      C     Dm
I am the man who walks alone

```


## Janusz Radek - Kiedy U-kochanie

[Back to Table of Contents](#user-content-table-of-contents)


```

[Chords]
D#dim7  X X 1 2 1 2
Adim7   X 0 1 2 1 2
 
 
[Intro]
Em G D A Em G D D#dim7
Em G D A Em G D D#dim7
Em G D A Em G D D#dim7
Em G D A Em G D N.C.
 
[Verse 1]
Em         G         D                  A
Kiedy umrę kochanie ,gdy się ze słońcem rozstanę
  Em               G             D D#dim7
i będę długim przedmiotem smutnym
Em             G             D         A
Czy mnie wtedy przygarniesz, ramionami ogarniesz
  Em                G          D D#dim7
naprawisz co popsuł los okrutny
 
[Verse 2]
G         D           Am       Em
Często myślę o tobie, często piszę do ciebie
G         D                   Am       Em
głupie listy - w nich miłość, głupie listy - w nich uśmiech
G         D           Am       Em
Często myślę o tobie, często piszę do ciebie
G         D                   Am                           Adim7
głupie listy - w nich miłość, głupie listy - w nich uśmiech
 
[Interlude]
Em      G       D  A
Na, na, na, na, na
Em      G       D  D#dim7
Na, na, na, na, na
Em      G       D  A
Na, na, na, na, na
Em      G       D
Na, na, na, na, na
 
[Verse 3]
Em            G          D              A
Potem w piecu je chowam, płomień skacze po słowach
Em                 G             D D#dim7
nim spokojnie w popiele nie uśnie
Em                G         D             A
Patrząc w płomień kochanie, myślę, co też się stanie
 Em              G           D D#dim7
z moim sercem miłości głodnym
 
[Verse 4]
G        D                Am     Em
A ty nie pozwól przecież, żebym umarł w świecie
G        D               Am          Em
który ciemny jest, który ciemny jest i chłodny
G        D                Am     Em
A ty nie pozwól przecież, żebym umarł w świecie
G        D               Am                   Adim7
który ciemny jest, który ciemny jest i chłodny
 
[Interlude]
Em G D A Em G D D#dim7
Em G D A Em G D D#dim7
 
[Verse 5]
Em         G         D                  A
Kiedy umrę kochanie ,gdy się ze słońcem rozstanę
  Em               G             D D#dim7
i będę długim przedmiotem smutnym
Em             G             D         A
Czy mnie wtedy przygarniesz, ramionami ogarniesz
  Em                G          D D#dim7
naprawisz co popsuł los okrutny
 
[Verse 6]
G         D           Am       Em
Często myślę o tobie, często piszę do ciebie
G         D                   Am       Em
głupie listy - w nich miłość, głupie listy - w nich uśmiech
G        D                Am     Em
A ty nie pozwól przecież, żebym umarł w świecie
G        D               Am                   Adim7
który ciemny jest, który ciemny jest i chłodny
 
[Outro]
Em      G       D  A
Na, na, na, na, na
Em      G       D  D#dim7
Na, na, na, na, na
Em      G       D  A
Na, na, na, na, na
Em      G       D D#dim7 Em
Na, na, na, na, na


```


## John Legend - All of me

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]

Fm C# G# D#

[Verse 1] 

Fm           C#maj7                  G#  
What would I do without your smart mouth
           D#                      Fm
Drawing me in, and you kicking me out
          C#            G#               D#          Fm
Got my head spinning, no kidding, I can't pin you down
            C#maj7                 G#
Whats going on in that beautiful mind
               D#             Fm
Im on your magical mystery ride
       C#add9             G#                     D#         A#madd9
And Im so   dizzy, don't know what hit me, but I'll be alright

[Pre-Chorus]

A#m                G#
My head's under water
    D#             A#madd9
But Im breathing fine
                G#             D#
Youre crazy and Im out of my mind

[Chorus]

       G#
Cause all of me
       Fmadd9
Loves all of you
          A#m/C#
Love your curves and all your edges
           C#/D#        D#
All your perfect imperfections
           G#
Give your all to me
            Fmadd9
Ill give my all to you
         A#m/C#
Youre my end and my beginning
     C#/D#             D#
Even when I lose I'm winning
                  Fm   C#      G#   D#add9
Cause I give you all, all of me
                 Fm   C#      G#    D#add9
And you give me all, all of you, oh

[Verse 2] 

Fm       C#                       G#
How many times do I have to tell you
                D#                       Fm
Even when youre crying you’re beautiful too
              C#           G#          D#                  Fm
The world is beating you down, I'm around through every mood
            C#                G#
Youre my downfall, youre my muse
             D#                        Fm
My worst distraction, my rhythm and blues
               C#            G#        D#            A#m
I cant stop singing, its ringing, in my head for you

[Pre-Chorus]

A#m                G#
My head's under water
    D#             A#madd9
But Im breathing fine
                G#             D#
Youre crazy and Im out of my mind


[Chorus]
       G#
Cause all of me
       Fmadd9
Loves all of you
          A#m/C#
Love your curves and all your edges
           C#/D#        D#
All your perfect imperfections
           G#
Give your all to me
            Fmadd9
Ill give my all to you
         A#m/C#
Youre my end and my beginning
     C#/D#             D#
Even when I lose I'm winning
                  Fm   C#      G#   D#add9
Cause I give you all, all of me
                 Fm   C#      G#    D#add9
And you give me all, all of you, oh

[Pre-Chorus Change]

A#m            G#
Cards on the table
       D#             A#madd9
Were both showing hearts
A#m          G#               D#
Risking it all, though its hard

[Chorus]

       G#
Cause all of me
       Fmadd9
Loves all of you
          A#m/C#
Love your curves and all your edges
           C#/D#        D#
All your perfect imperfections
           G#
Give your all to me
            Fmadd9
Ill give my all to you
         A#m/C#
Youre my end and my beginning
     C#/D#             D#
Even when I lose I'm winning
                  Fm   C#      G#   D#add9
Cause I give you all, all of me
                 Fm   C#      G#    D#add9
And you give me all, all of you, oh

[Outro]
                  Fm   C#      G#   D#
Cause I give you all, all of me
                 Fm   C#      G#    D#
And you give me all, all of you, oh

```


## Kombii - Ślad

[Back to Table of Contents](#user-content-table-of-contents)


```

[Chords]
Amadd9  X 0 2 4 1 0
 
 
 
[Intro]
Amadd9 Fmaj7 G
Am Dm7 Em7 x4
 
[Verse 1]
Amadd9             Dm7   Em7 Amadd9             Dm7   Em7
      Żyjemy przez chwilę,         zachłanni na każdy gest,
Fmaj7          Cmaj7            Dm7          Esus4
Tylko w twoich oczach widzę ten blask, życia blask.
Amadd9             Dm7    Em7 Amadd9             Dm7    Em7
      Ukryci przed światem          wierzymy, że stanął czas,
Fmaj7        Cmaj7            Dm7            Esus4
Nasza miłość nie  przeminie i trwać będzie w nas.
 
[Chorus]
        Am               Fmaj7  G
Na dnie serca pozostanie zawsze ślad,
      Am                   Fmaj7  G
Kiedy ciebie już nie będzie tu,
        Am               Fmaj7  Em7
Na dnie serca pozostanie zawsze ślad,
  Dm7    Esus4
Zachowam go.
 
[Solo 1]
Am Dm7 Em7 x2
 
[Verse 2]
Amadd9             Dm7    Em7 Amadd9
      Powrotów nie będzie,
            Dm7     Em7
Co było nie wróci,  wiem.
Fmaj7         Cmaj7
Nie  spotkamy się  już więcej,
   Dm7             Esus4
To los, tak chciał los.
 
[Chorus]
        Am               Fmaj7  G
Na dnie serca pozostanie zawsze ślad,
      Am                   Fmaj7  G
Kiedy ciebie już nie będzie tu,
        Am               Fmaj7  Em7
Na dnie serca pozostanie zawsze ślad,
  Dm7    Esus4
Zachowam go.
       Am               Fmaj7  G
Nie zapomnę nigdy smaku twoich ust,
       Am                Fmaj7 G
Nie zapomnę barwy twoich słów,
        Am                     Fmaj7   Em7  Dm7   Esus4
Na dnie serca parę wspomnień z tamtych dni ocalić chcę,
        Amadd9 Fmaj7 G       Amadd9 Fmaj7 G
Chociaż to,            tylko to.
 
[Solo 2]
Am Dm7 Em7 x3
Fmaj7 Esus4
 
[Chorus]
        Am               Fmaj7  G
Na dnie serca pozostanie zawsze ślad,
      Am                   Fmaj7  G
Kiedy ciebie już nie będzie tu,
        Am               Fmaj7  Em7
Na dnie serca pozostanie zawsze ślad,
  Dm7    Esus4
Zachowam go.
       Am               Fmaj7  G
Nie zapomnę nigdy smaku twoich ust,
       Am                Fmaj7 G
Nie zapomnę barwy twoich słów,
        Am                     Fmaj7   Em7  Dm7   Esus4
Na dnie serca parę wspomnień z tamtych dni ocalić chcę,
        Am
Chociaż to,
 
[Interlude]
Am Dm7 Em7 x2
        Am
Chociaż to
 
[Outro]
Am Dm7 Em7 x2
Amadd9

```


## Lianne la havas - Ghost

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro/Outro]

e|------------------------------------------------|
B|------------------------------------------------|
G|--5----h7--p5--h7--p5------5---h7--p5----5------|
D|--5------------------------5-------------4------|   
A|--6--6------------------6------------6---5------|
E|------------------------------------------------|(x2)


[Verse]

e|--------------5---1--------------------------------------------------------|
B|-(6)----------3---1---13---------------10---8------------------------------|
G|--5---5---5---3---1---13---5---5---5---10---7------------------------------|
D|--5---5---4---3---1---12---5---5---4---8----7------------------------------|
A|--6---5---5---5---3---13---6---5---5---10---8------------------------------|
E|--------------3---1--------------------------------------------------------|

e|--------------5---1--------------------------------------------------------|
B|--------------3---1---13---------------10---3------------------------------|
G|--5---5---5---3---1---13---5---5---5---10---3------------------------------|
D|--5---5---4---3---1---12---5---5---4---8----2------------------------------|
A|--6---5---5---5---3---13---6---5---5---10----------------------------------|
E|--------------3---1-------------------------3------------------------------|


[Chorus]

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|--3---3---3---5---------------5--------------------------------------------|
D|--1---1---1---5---5---0---5---5--------------------------------------------|
A|--3-------3---6---5---0---5------------------------------------------------|
E|------3-----------6--\1--/6---5--------------------------------------------|

************************************

| (n)  Ghost note
| h    Hammer-on
| p    Pull-off
| /    Slide up
| \    Slide down

Whenever I called you
I couldn't say
It was only yesterday
And yesterday's so far away
What's the ETA? [?]

Don't tell me you need me
I am estranged
And I'm over-aged [?]
The trouble it may be
There's still a part of me
That has to know
What you have to say

[Chorus]
On and on we go
Always with the ghosts of us in tow
Stuck somewhere between a friend and foe
Round and round we go

Oh, round and round again
Looking for a life beyond the end
Lost somewhere between a foe and friend
Round and round again

I should have told you
Yes, I'm to blame
Oh, I took the blame
Turned it into this serenade
Oh, the mess I've made

And I should have warned you
What was in store
But I was so so sure
I wasn't haunted anymore
Not forevermore

[Chorus x2]
On and on we go
Always with the ghosts of us in tow
Stuck somewhere between a friend and foe
Round and round we go

Oh, round and round again
Looking for a life beyond the end
Lost somewhere between a foe and friend
Round and round again

On and on we go
Always with the ghosts of us in tow
Stuck somewhere between a friend and foe
Round and round we go

Oh, round and round again
Looking for a life beyond the end
Lost somewhere between a foe and friend
Round and round again
```


## Lianne La Havas - Green and Gold

[Back to Table of Contents](#user-content-table-of-contents)


```

intro
Cmaj7-C6, d7, Cmaj7-C6, d7, C#no5,
Six years old
Staring at my nose through the mirror
Trying to get my toes in the mirror
Thinking 'Who's that girl?'
And 'Does the mirror world go on forever?'
Calmly you roll
Sharpening the knives in the attic
Trying to watch cartoons through the static
Thinking where am I gonna be
If I'm ever twenty three?

I'm looking at a life unfold
Dreaming of the green and gold
Just like the ancient stone
Every sun rise I know
Those eyes you gave to me
That let me see
Where I come from

Found an old friend
Meeting my guitar in the city
Feeling like a star in the city
And suddenly it seems that I'm where I'm supposed to be, oh

And now I'm fully grown
//notes: a-e-g-a-h
And I'm seeing everything clearer
//notes: g-f#-a-h-d
Just sweep away the dust from the mirror
//notes: a-e-g-a-h
We're walking hand in hand in the warm white sands
//notes: g-f#-a-h-d

I'm looking at a life unfold
Dreaming of the green and gold
Just like the ancient stone
Every sun rise I know
Those eyes you gave to me
That let me see
Where I come from

Ancient stone
Oh, green and gold
Ancient stone
Oh, green and gold
Ancient stone
Green and gold
Ancient stone
Green and gold

I'm looking at a life unfold
Dreaming of the green and gold
Just like the ancient stone
Every sun rise I know
Those eyes you gave to me
That let me see
Where I come from
```


## Lianne la havas - is your love big enough

[Back to Table of Contents](#user-content-table-of-contents)


```

#----------------------------------PLEASE NOTE-------------------------------#
#This file is the author's own work and represents his interpretation of the #
#song. You may only use this file for private study, scholarship or research.#
#----------------------------------------------------------------------------#

Song title:       Is Your Love Big Enough?
Original Album:   Is Your Love Big Enough?

Words and Music by Lianne La Havas
Copyright � 2012 Warner Brothers Records
Published by Warner Chappell

Transcribed by Gavin Chart (contactus@gartfrets.net)


NOTES:
This has a Latin feel to it and is already a live favourite. It was
transcribed from a live performance at Point Ephemere, Paris, 6th March 2012.
Enjoy!

Guitar Figure 1 is the basis of the verses and is repeated.
Guitar Figure 2 is played in the pre-chorus and is repeated.

B* is called B 3rd interval
E* is called E 3rd interval
A* is called A 3rd interval
D* is called D 3rd interval

Chords:
B*   x 14 13 xxx
E*   x76xxx
A*   x 12 11 xxx
D*   x54xxx
E*   x76xxx


Guitar Figure 1

    B*                                 E*                      A*
|-------------------------------------------------------------------|
|-------------------------------------------------------------------|
|*-----------------------------------------------------------------*|
|*--13------13------13------13---------6-----6-----6-----6-----11--*|
|---14----------14------14------14--9--7--------7-----7-----7--12---|
|-------14--------------------------------7-------------------------|


Guitar Figure 2

     D* E*
|--------------------------------|
|--------------------------------|
|*------------------------------*|
|*---4--6-----6-----6-----6-----*|
|----5--7--------7-----7-----7---|
|----------7---------------------|



INTRO:

[play: Guitar Figure 1]  [x2]


VERSE:
[Guitar Figure 1]
Found myself in the second-
[Guitar Figure 1]
I found myself in the second-hand guitar
[Guitar Figure 1]
Never thought it would happen
[Guitar Figure 1]
But I found myself in the second-hand guitar


PRE-CHORUS:
[Guitar Figure 2]
    So I've just got to know
[Guitar Figure 2]
    I truly have to know
[Guitar Figure 2]           E*
    So you've got to let me know


CHORUS:
       [Guitar Figure 1]    [Guitar Figure 1] 
Is your love            big enough for what's to come?
[Guitar Figure 1]                 [Guitar Figure 1] 
  Got so hot in the city that I forgot everything I was looking for
[Guitar Figure 1]                     [Guitar Figure 1] 
  Make my way to the dance floor and I danced 'til I wasn't drunk anymore


PRE-CHORUS:
[Guitar Figure 2]      [Guitar Figure 2]
    So I've just got to know
               [Guitar Figure 2]
I truly have to know
                    [Guitar Figure 2]
So you've got to let me know


CHORUS:
       [Guitar Figure 1]    [Guitar Figure 1]
Is your love            big enough for what's to come?
       [Guitar Figure 1]
Is your love big enough?


BRIDGE:
Chords:
D6b5     7x677x
C#b5     6x566x

N.C.
Underground, underground
With the friends I've found, friends I've found

D6b5         C#6b5 - D6b5
Underground, under - ground
         D6b5         C#6b5 - D6b5  
With the friends I've found,  friends I've found


[play: Guitar Figure 1]  [x2]


[Guitar Figure 1]
I scream
[Guitar Figure 1]
I scream
[Guitar Figure 1]
On Second Avenue
[Guitar Figure 1]
I scream on Second Avenue


CHORUS:
Is your love big enough for what's to come?
Is your love big enough?
Is your love big enough for what's to come?
Is your love big enough for what's to come?


[play:  D6b5  C#b5 - D6b5]  [x2]

[end on D6b5]
```


## Lianne la Havas - No room for doubt

[Back to Table of Contents](#user-content-table-of-contents)


```

Lianne La Havas – No Room For Doubt 
Lianne Charlotte Barnes, Matt Hales, Willy Mason 
Fm7 – EX A6 D6 G5 H6 EX
Gm7 – EX A8 D8 G7 H8 EX
Bb7 – E6 A8 D6 G7 (H6) (E6)

Intro 
| Fm/Ab | Gm/Bb | Fm/Ab | Fm/Ab | 
 
Verse 1 
Fm/Ab 
 You caught me, guilty 
Gm/Bb                 Fm/Ab   Gm/Bb 
 Taking the pieces of you 
Fm/Ab 
 That night, you took flight 
Gm/Bb                      Fm/Ab   Gm/Bb 
 I couldn't decide what to do 
Fm/Ab 
 I won't let a safe bet 
Gm/Bb                     Fm/Ab   Gm/Bb 
 Continue to make me feel blue 
Fm/Ab 
 I could go solo 
Gm/Bb                            Fm7   Bb7 
Would that be the right thing to do? 
 
Chorus 
   Fm/Ab      Gm/Bb       Fm7 
We all make mistakes, we do 
              Bb7 
I learnt from you 
   Fm/Ab      Gm/Bb       Fm7 
We all make mistakes, we do 
              Bb7 
I learnt from you 
 
Verse 2 
I tiptoe, too slow 
Out of the door to your house 
I know you know 
That this was leads me out 
Outside, too bright 
You're within I'm without You're within 

Play Chorus 
Bridge 
Cm7 
 Please sleep softly 
Ab                    Gm7 - Bb7 
 Leave me no room for doubt 
Cm7 
 Please sleep softly 
Ab                    Gm7 - Fm7 
 Leave me no room for doubt 
Cm7 
 Please sleep softly 
Ab                    Gm7 - Bb7 
 Leave me no room for doubt 
Cm7 
 Please sleep softly 
Ab                    Gm7 
 Leave me no room for doubt 
Gm7       Gm7-Gbm7     Fm7   Bb7 
 Leave me no room for doubt 
 
Play Chorus x2 
 
Outro 
Cm7 
 Please sleep softly 
Ab                    Gm7 - Bb7 
 Leave me no… 
Cm7 
 Please sleep softly 
Ab                    Gm7 - Fm7   Cm7  Leave me no room for doubt 
```


## Lianne la havas - Wonderful

[Back to Table of Contents](#user-content-table-of-contents)


```

[Chords used]

F#m     244222
E       022100
C#m9    x4244x
F#m9    x9799x
B7/F#   x9787x
Emaj9   07687x
E6add9  07667x


[Verse 1]

F#m
   Did the world get a little bit colder
E
   Not wiser just a little bit older
C#m
   So slow that we're bound to fall over, oh

F#m
   Did the heart grow a little bit harder
E                                   C#m
   Too much, too late, too far, too gone

    F#m
But wasn't it kind of wonderful
E
   Wasn't it kind of wonderful, baby
C#m9
   Wasn't it kind of wonderful, wonderful


[Chorus]

        F#m9
You can trip, flick a switch, negative
                    B7/F#
Break the circuit between us
                Emaj9
But electricity lingers
       E6add9
In our fingers

        F#m9
Oh you can burn every fuse and refuse
                   B7/F#
Turn your positive minus
            Emaj9
Electricity lingers
       E6add9
In our fingers


[Verse 2]

F#m
   From here there's nothing but horizon
E
   Near dawn, I'm searching for the sunrise
C#m9
   Remember when you put the stars into my eyes, oh

F#m
   Wasn't it kind of wonderful
Emaj9
   Wasn't it kind of wonderful, baby
C#m9
   Wasn't it kind of wonderful, wonderful


[Chorus]

        F#m9
You can trip, flick a switch, negative
                    B7/F#
Break the circuit between us
                Emaj9
But electricity lingers
       E6add9
In our fingers

        F#m9
Oh you can burn every fuse and refuse
                   B7/F#
Turn your positive minus
            Emaj9
Electricity lingers
       E6add9
In our fingers

F#m9
   Wasn't it kind of wonderful
B7/F#
   Wasn't it kind of wonderful, baby
Emaj9                                    E6add9
   Wasn't it kind of wonderful, wonderful

F#m9
   Wasn't it kind of wonderful
B7/F#
   Wasn't it kind of wonderful, baby
Emaj9                                    E6add9
   Wasn't it kind of wonderful, wonderful
```


## Linkin Park - Final Masquerade

[Back to Table of Contents](#user-content-table-of-contents)


```

Linkin Park - Final Masquerade
From the album: The Hunting Party (2014)
Comment for any corrections and please rate
 
 
Capo 1
 
[Intro:]
Bm D Bm D
 
 
[Verse]
             Bm                           D
Tearing me apart with words you wouldn't say
            Bm                      D
Suddenly tomorrow's moment washed away
                    G                             Bm
Cuz I don't have a reason and you don't have the time
                     A                              F#
But we both keep on waiting for something we won't find
 
 
[Chorus]
                    G                       D
The light on the horizon is brighter yesterday
                 A                     Bm
Shadows floating over, scars begin to fade
                  G                          D
We said it was forever but then it slipped away
                A                       Bm
Standing at the end of the final masquerade
                 Bm D Bm D
The final masquerade
 
 
[Verse]
               Bm                           D
All I've ever wanted, the secrets that you keep
                 Bm                            D
All you've ever wanted, the truth I couldn't speak
                    G                                Bm
Cuz I can't see forgiveness, and you can't see the crime
                    A                          F#
If we both keep on waiting for what we left behind
 
 
[Chorus]
                    G                       D
The light on the horizon is brighter yesterday
                 A                     Bm
Shadows floating over, scars begin to fade
                  G                          D
We said it was forever but then it slipped away
                A                       Bm
Standing at the end of the final masquerade
                 G                     D
The final masquerade, the final masquerade
                A                       Bm
Standing at the end of the final masquerade
 
 
[Instrumental]
G D A Bm
G D A Bm
 
 
[Chorus]
                    G                        D
The light on the horizon was brighter yesterday
                      A                     Bm
With shadows floating over, scars begin to fade
                  G                          D
We said it was forever but then it slipped away
                A                       Bm  G D
Standing at the end of the final masquerade
                A                       Bm  G D
Standing at the end of the final masquerade
                A                       Bm
Standing at the end of the final masquerade
                 G
The final masquerade

```


## Linkin Park - In the end

[Back to Table of Contents](#user-content-table-of-contents)


```

In The End
Linkin Park

Tune down a half-step! Chords are relative to standard tuning (will sound 1/2-step lower)


[Verse 1]

                    D#m
It starts with One thing I don't know why
        C#
It doesn't even matter how hard you try
        B
Keep that in mind I designed this rhyme
        C#
To explain in due time
        D#m
All I know Time is a valuable thing
        C#
Watch it fly by as the pendulum swings
        B
Watch it count down to the end of the day
        C#
The clock ticks life away
          D#m
It's so unreal Didn't look out below
        C#
Watch the time go right out the window
        B
Trying to hold on but didn't even know
        C#
Wasted it all just to
                D#m
Watch you go I kept everything inside 
                C#
And even though I tried it all fell apart
                B
What it meant to me will eventually 
            C#
Be a memory of a time when


[Chorus]

           D#m              F#               C#                    B
I tried so hard And got so far But in the end It doesn't even matter
           D#m              F#               C#                    B
I had to fall To lose it all But in the end It doesn't even matter


[Verse]

D#m
One thing I don't know why
   C#
It doesn't even matter how hard you try
B
Keep that in mind I designed this rhyme
     C#
To remind myself how
           D#m
I tried so hard In spite of the way you were mocking me
C#
Acting like I was part of your property
  B
Remembering all the times you've fought with me
        C#                D#m
I'm surprised it got so (far)
D#m
Things aren't the way they were before
C#
You wouldn't even recognize me anymore
B
Not that you knew me back then
       C#
But it all comes back to me
        D#m
(In the end)
You kept everything inside 
    C#
And even though I tried it all fell apart
B
What it meant to me will eventually 
C#
Be a memory of a time when 


[Chorus]
           D#m              F#               C#                    B
I tried so hard And got so far But in the end It doesn't even matter
           D#m              F#               C#                    B
I had to fall To lose it all But in the end It doesn't even matter
 

[Verse]

             D#m    C#            B              C#
I put my trust in you Pushed as far as I can go
             D#m               C#                       B   C#
And for all this There's only one thing you should know

             D#m    C#            B              C#
I put my trust in you Pushed as far as I can go
             D#m               C#                       B   C#
And for all this There's only one thing you should know


[Chorus]

           D#m              F#               C#                    B
I tried so hard And got so far But in the end It doesn't even matter
           D#m              F#               C#                    B
I had to fall To lose it all But in the end It doesn't even matter
```


## Lor - Nikt

[Back to Table of Contents](#user-content-table-of-contents)


```

A#m       G# 
Pamiętam jak myśleliśmy
D#           D#sus4       D#
Że świat nas nie dotyczyłbD#y  
A#m       G# 
Gdybyśmy tak na przekór im
D#     D#sus4 D#
Pozakrywali oczy. 


A#m        G#
Już prawie cię nie widzę.
D#         D#sus4         D#
Spróbuj odgadnąć kto jest kim.
A#m      G#
Jesteśmy coraz bliżej.
D#     D#sus4  D#
Pozakrywajcie oczy.

A#m
Niech nikt się nie
G#
Porusza, nikt mi nie
D#
przerywa. Zacznę gdy zapadnie zmrok.
A#m
Nikogo już nie
G#
słucham, nikt się nie
D#
odzywa. Radzę wam odwróćcie wzrok.

A#m      G#
Pamiętam, jak myśleliśmy
D#          D#sus4    D#
Że nikt nas nie zobaczyłby
A#m      G#
Gdybyśmy tak ostatkiem sił
D#   D#sus4 D#
Pozakrywali oczy.

A#mJuż prawie G#cię nie widzę.
D#Spróbuj D#sus4odgadnąć kto jeD#st kim.
A#mJesteśmy G#coraz bliżej.
D#PozakrD#sus4ywajcie oD#czy.

A#mNiech nikt się nie
G#Porusza, nikt mi nie
D#przerywa. Zacznę gdy zapadnie zmrok.
A#mNikogo już nie
G#słucham, nikt się nie
D#odzywa. Radzę wam odwróćcie wzrok.

GbUdawaj G#że mnie nie ma. D#   

GbUdawaj G#że zniknęłam.  A# 

A#mNiech nikt się nie
G#Porusza, nikt mi nie
D#przerywa. Zacznę gdy zapadnie zmrok.
A#mNikogo już nie
G#słucham, nikt się nie
D#odzywa. Radzę wam odwróćcie wzrok.

A#mNiech nikt się nie
G#Porusza, nikt mi nie
D#przerywa. Zacznę gdy zapadnie zmrok.
A#mNikogo już nie
G#słucham, nikt się nie
D#odzywa. Radzę wam odwróćcie wzrok.

A#mNiech nikt się G#nie porusza.D#   
A#mNikogo G#już nie słucham.D#   

```


## Marek Grechuta - Ocalić od zapomnienia

[Back to Table of Contents](#user-content-table-of-contents)


```

 		c g f c
 		f6+/D c/Eb f6+ c/Eb f G
 		Ab g f G
 		c f6+/D c/Eb f6+/D
 		c f6+/D c/Eb f6+
 		
Ile razem dróg przebytych 		c g
Ile scieżek przedeptanych? 		f c f6+/D
Ile deszczów, ile śniegów 		c/Eb f6+
Wiszących nad latarniami? 		c/Eb f G
Ile listów, ile rozstań 		Ab g
Ciężkich godzin w miastach wielu? 	f G
I znów upór, żeby powstać 		c f6+/D
I znów iść i dojść do celu. 	c/Eb f6+/D
 		
 		c f6+/D c/Eb f6+
 		
Ile w trudzie nieustannym 		c g
Wspólnych zmartwień, wspólnych dążeń? 	f c f6+/D
Ile chlebów rozkrajanych? 		c/Eb f6+
Pocałunków, schodów, książek?  	c/Eb f G
Oczy twe jak piękne świece, 	Ab g
A w sercu źródło promienia, 	f G
Więc ja chciałbym Twoje serce  	c f6+/D
Ocalić od zapomnienia. 		c/Eb f6+/D
 		
 		c f6+/D c/Eb f6+
 		
U twych ramion płaszcz powisa 		c g
Krzykliwy, z leśnego ptactwa, 		f c f6+/D
Długi przez cały korytarz, 		c/Eb f6+
Przez podwórze, aż gdzie gwiazda Wenus. c/Eb f G
A tyś lot i górność chmur, 		Ab g
Blask wody i kamienia. 			f G
Chciałbym oczu twoich chmurność 	c f6+/D
Ocalić od zapomnienia. 			c/Eb f6+/D
 		
(...) 		c f6+/D c/Eb f6+/D
```


## Paramore - In The mourning

[Back to Table of Contents](#user-content-table-of-contents)


```


[Verse 1]

G                Bm        Esus2
You escape like a runaway train
G                           Bm     Esus2
Off the tracks and down again
G                                    Bm             Esus2
My heart's beating like a steamboat tugging.
G     Bm    D  Bm - G   Bm  
D      Bm
All your burdens, on my shoulders.


[Chorus]

(Bm)         G     D      A
In the morning, I'll rise.
(A)           G     D      A
In the mourning, I'll let you die.
(A)      G   D   Em
In the mourning,
(Em)   G  Bm  D
All my worry.


[Verse 2]

G                                          Bm             Esus2
And now there's nothing but time that's wasted
G                                   Bm        Esus2
And words that have no backbone
G                               Bm                  Esus2
Now it seems like the whole world’s waiting
G  Bm   D   Bm -- G   Bm  D   Bm
Can you hear the echoes fading?


[Chorus]

(Bm)    G            D         A
In the morning, I’ll rise.
(A)      G          D              A
In the mourning, I’ll let you die
(A)     G     D      Em
In the mourning,
          G  Bm D
All my sorry.


[Verse 3]

 (D)  Bm       G         D
And it takes all my strength
(D)                  Em               Bm                      
      A
Not to dig you up from the ground, in which you lay.
(A)                          Em
The biggest part of me
(Em)             D
You were the greatest thing
(D)   Bm                        Em
And now you are just a memory
(Em) G   Bm   D
To let go of.


[Chorus]

(D)    G            D         A
In the morning, I’ll rise.
(A)      G          D              A
In the mourning, I’ll let you die
(A)     G     D      Em
In the mourning,
          G   Bm  D
All my sorry.
```


## Sam Brown - Stop

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]
  Em    Em/B Em/D   Em   Em/B Em/D   Bm  Bm/F# Bm/A  Bm
|  /  /   /   /  |  /  /   /   /  |  /  /  /   /  |

[Verse 1]
N.C.                                Em       Em/B Em/D Em
All that I have is all that you've given me,     (Oo - oo!)
              Em/B  Em/D           Bm                   Bm/F# Bm/A Bm
Did you never worry...   that I'd come to depend on you?    (Oo - oo!)
           Bm/F#    Bm/A       Em     Em/B Em/D Em
I gave you all the love I had in me,
                    F#m   F#m/C#  F#m/A  F#m  Bm
And now I find you lied,
                  F#m                F#m/A  Bm
And I can't be - lieve it's true!


[Verse 2]
N.C.                                Em
Wrapped in her arms, I see you a - cross the street,
Em/B    Em/D         Em
     (I see you a - cross the street!)
                     Em/B  Em/D        Bm                   Bm/F# Bm/A Bm
And I can't help but won - der if she knows what's going on,    (Ooo)
                Bm/F#      Bm/A           Em            Em/B Em/D Em
Oh, you talk of love, but you don't know how it feels,
                F#m   F#m/C#  F#m/A  F#m  Bm                      F#m      F#m/A
When you rea - lise...                  that you're not the only one!


[Chorus]
Bm     A/C#        D     D/F#    G
Oh---, oh - oh, you'd better stop...  (Stop!)
         F#        Bm             E7
...Be - fore you tear me all a - part,
               G
You'd better stop...  (Stop!)
         F#          Bm    A/C#      D     D/F#     E7
...Be - fore--- you go... and... break... my... heart...
 G           A                 Bm    F#m F#m/A  Bm
Hoo---, oo - oo, you'd better stop!


[Verse 3]
N.C.                           Em       Em/B  Em/D          Em
Time after time I've tried to walk away,     (I've tried to walk away!)
                 Em/B  Em/D              Bm             Bm/F#   Bm/A  Bm
But it's not that ea - sy  ...when your soul, is torn in two,  (Oo - oo!)
               Bm/F#      Bm/A         Em     Em/B    Em/D         Em
So I just re - sign my - self...to it...every---- day----,(Woh, everyday!)
              F#m  F#m/C# F#m/A F#m    Bm                  F#m   F#m/A
Now all I can do...     is  to...leave it up to you!  Woh - oh...


[Chorus]
Bm     A/C#        D     D/F#    G
Oh---, oh - oh, you'd better stop...  (Stop!)
         F#        Bm             E7
...Be - fore you tear me all a - part,
               G
You'd better stop...  (Stop!)
         F#          Bm    A/C#      D     D/F#     E7
...Be - fore--- you go... and... break... my... heart...
 G          A                 Em              D
Hoo---,oo - oo, you'd better stop... if you love me,
                G             D                Em               F#m
     Now's the time...to be sorry,             I won't...be - lieve that...
(You will re - member...     ...that day for - ever!)

          G          A        A4   A            Bm   F#m    F#m/A  Bm
...You'd walk out on me----!  Ba - by,  yeah - yeah-----!        Oo---oo!


[Break]
  Em   Em/B Em/D   Em   Em/B Em/D   Bm  Bm/F# Bm/A  Bm A/C# D D/F#
|  /  /  /   /  |  /  /   /   /  |  /  /  /  /  |  /  /  /  /  |

  Em   Em/B Em/D   Em   Em/B Em/D   Bm  Bm/F# Bm/A
|  /  /  /   /  |  /  /   /   /  |  /  /  /  /  |


[Chorus]
Bm     A/C#        D     D/F#    G
Oh---, oh - oh, you'd better stop...  (Stop!)
         F#        Bm              E7
...Be - fore you tear me all a - part,
               G
You'd better stop...  (Stop!)
         F#          Bm    A/C#      D     D/F#     E7     N.C.
...Be - fore--- you go... and... break... my... heart...


[Coda]
  G          A                 Em               A
 Hoo---,oo - oo! You'd better stop!      Baby, stop!
(Hoo---,oo - oo! You'd better...  you'd better...  you'd better...)

  G           A                Em                A
 Hoh---,oo - oh! You'd better...  stop!    Oh, stop!
(Hoo---,oo - oo!  You'd better... you'd better...    you'd better...)

        G            A                  G                    A
Baby, stop!  Baby, stop!
      (Hoo---oo,oo -oo!  You'd better...    you'd better...)

              G            A                     G                 A
You'd better stop,   stop,   stop! You'd better stop!
            (Hoo---, oo - oo!      You'd better...  you'd better...)

          G             A                 G                    A
Yeah---- stop!                                Hoo - oo!
         (Hoo---, oo - oo!  You'd better...   you'd better...)


[Suggestion for ending]
              G            A                         G        A   Bm
You'd better stop!            You'd... bet...ter... stop----!
            (Hoo---, oo - oo! You'd... bet...ter... stop----!)

```


## Snarky Puppy, Laura Mvula - Sing to the Moon

[Back to Table of Contents](#user-content-table-of-contents)


```

Author
Difficulty
stang7 168
intermediate

Strumming
There is no strumming pattern for this song yet. Add it and get +5 IQ
[Intro]

Eb6/Bb F6/9 | G7sus F/A | Bb6/9 Eb\Bb Dm | Gm


[Verse]

Cm7 Dm7   Ebmaj7  Gm7            F        Ebmaj9
Hey there you,    shattered in a thousand pieces
               Cm7     G5
Weeping in the darkest nights.
Cm7 Dm7   Ebmaj7          Gm7             F       Ebmaj9 
Hey there you,    try to stand up on your own two feet
            Cm7         G5 
And stumble through the sky.


[Pre-Chorus]

Cm7 Dm7 Ebmaj7     Gm7       F              Ebmaj9
          When the lights go out and you’re on your own
Cm7 Dm7 Ebmaj7             Gm7     F                Ebmaj7
          How you’re gonna make it through till the morning sun?


[Chorus]

Dm7        Ebmaj7        F          Gm
Sing to the moon and the stars will shine
F Gm Ebmaj7                 F     Gm
Over you,   lead you to the other side.
Dm7        Ebmaj7        F          Gm
Sing to the moon and the stars will shine
F Gm Ebmaj7              F        Gm
Over you, heaven’s gonna turn the time.


[Verse]

Cm7 Dm7   Ebmaj7  Gm7            F        Ebmaj9
Hey there you, looking for a brighter season
               Cm7     G5
Need to lay your burden down.
Cm7 Dm7 Ebmaj7 Gm7             F       Ebmaj9 
Hey there you, drowning in a hopeless feeling,
             Cm7    G5 
Buried under deeper ground.


[Pre-Chorus]

Cm7 Dm7 Ebmaj7     Gm7       F         Ebmaj9
        When the lights go out it’s a waiting game.
Cm7 Dm7 Ebmaj7             Gm7     F                Ebmaj7
        Never gonna see a day when your world will change.

 
[Chorus]

Dm7        Ebmaj7        F          Gm
Sing to the moon and the stars will shine
F Gm Ebmaj7                 F     Gm
Over you,   lead you to the other side.
Dm7        Ebmaj7        F          Gm
Sing to the moon and the stars will shine
F Gm Ebmaj7              F        Gm
Over you, heaven’s gonna turn the time.

Dm7        Ebmaj7        F          Gm
Sing to the moon and the stars will shine
F Gm Ebmaj7                 F     Gm
Over you,   lead you to the other side.
Dm7        Ebmaj7        F          Gm
Sing to the moon and the stars will shine
F Gm Ebmaj7              F        Gm
Over you, heaven’s gonna turn the time.
```


## SOAD - Lonely Day

[Back to Table of Contents](#user-content-table-of-contents)


```

Tune a Half-Step Down!
 
[Intro]
Am F C E7 x2
 
[Verse 1]
 
Am        F          C         E7
   Such a lonely day, and it's mine
Am          F                   C    E7
   The most loneliest day of my life
Am        F          C          E7
   Such a lonely day, should be banned
Am        F                C     E7
   It's a day that I can't stand
 
 
[Chorus]
 
Am          F                   C   E7
   The most loneliest day of my life
Am          F                   C   E7
   The most loneliest day of my life
 
 
[Verse 2]
 
Am        F          C           E7
   Such a lonely day, shouldn't exist
Am        F                   C   E7
   It's a day that I'll never miss
Am        F          C         E7
   Such a lonely day, and it's mine
Am          F                   C    E7
   The most loneliest day of my life 
 
 
[Bridge]
 
F            E7 G        Am
  And if you go, I wanna go with you
F            E7  G        Am
  And if you die, I wanna die with you
F            E7
  Take your hand and walk away
 
 
[Solo]
Am F C E7 x4
 
[Chorus]
 
Am          F                   C   E7
   The most loneliest day of my life
Am          F                   C   E7
   The most loneliest day of my life
Am          F                   C   E7
   The most loneliest day of my life
 
 
[Outro]
Am F C E7
 
Am        F          C         E7
   Such a lonely day, and it's mine
Am        F                      C  E7
   It's a day that I'm glad I survived


```


## SOAD - Roulette

[Back to Table of Contents](#user-content-table-of-contents)


```

zwrotka c-d# [c-moll], d#-g [D#], (g przej�ciowe), b-d [A#], c#-f [C#], (g) g#-c [G#], b-d [A#]
refren c-d# [c], g-h [G], b-c [A#sus2], (g), g#-c [G#], b-d [A#]

I have a problem that I can not explain
I have no reason why it should have been so plain
Have no questions but I sure have excuse
I lack the reason why I should be so confused

I know how I feel when I'm around you
I don't know
How I feel when I'm around you, around you

Left a message but it ain't a bit of use
I have some pictures but one might be the duce
Today you saw, you saw me you explained
Playin' the show when runnin' down the plain

I know how I feel when I'm around you
I don't know
How I feel when I'm around you
I, I know how I feel when I'm around you
I don't know
How I feel when I'm around you, around you
I, I know how I feel when I'm around you
I don't know
How I feel when I'm around you
I, I know how I feel when I'm around you
I don't know
How I feel when I'm around you, around you
Around you
Around you
Around you
```


## SOAD - Toxicity

[Back to Table of Contents](#user-content-table-of-contents)


```

-------------------------------------------------------------------------------
                            Toxicity - System of a Down
-------------------------------------------------------------------------------
Tabbed by: Jaromir
Email: lionfalcon@web.de
Capo 3rd fret

[Intro Riff]

e|---------------------------------------------------------------|
B|---------------------------------------------------------------|
G|---------------0-0-0-0-0-------------------0-2-2-0---0---------|
D|--2-2-2-2-2-2---------------2-2-2-3-3-2-2----------3-----------|
A|-0-0-0-0-0-0--3-3-3-3-3-3--0-0-0---0---0--3-3-3-3-3-3----------|
E|---------------------------------------------------------------|



[Intro]
Am C x2
(Riff)
Am Am F E
Am Am F E
Am Am F E
Am Am F E
Am C x2
(Riff)

[Verse 1]

Am          C
Conversion, software version 7.0
Am                                     C
looking at life through the eyes of a tired hub
Am                C
eating seeds as a pastime activity
Am                  C
the toxicity of our city, of our city

[Chorus]
Am
Now what do you own the world, how do you own
F         E
disorder, disorder
Am
Now somewhere between the sacred silence
F                  E
sacred silence and sleep
Am
Somewhere, between the
F                   E
sacred silence and sleep
Am
Disorder disorder,
F     E
disorder
 Am C
 Am C

[Verse 2]

Am                C
More wood for the fires, loud neighbors
Am                                C
Flashlight reveries caught in the headlights of a truck
Am                C
eating seeds as a pastime activity
Am                    C
the toxicity of our city, of our city

[Chorus] (x2)
Am
Now what do you own the world, how do you own
F         E
disorder, disorder
Am
Now somewhere between the sacred silence
F                  E
sacred silence and sleep
Am
Somewhere, between the
F                   E
sacred silence and sleep
Am
Disorder disorder,
F     E
disorder

[Outro]
Am      F          E
When I became the sun
Am                    F     E
I shone life into the man's hearts
Am      F          E
When I became the sun
Am                    F       E
I shone life into the man's hearts

```


## Son Lux - Easy

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]

Dm Dm Dm Bb Am

Dm Dm Dm Bb Am

[Verse 1]
Dm    Dm
Easy, easy
Dm
Pull out your heart
             Bb   Am
To make the being alone
Dm    Dm
Easy, easy
Dm
Pull out your heart
             Bb   Am
To make the being alone
Dm Am Dm  F
Easy, easy


[Interlude]

Dm Dm F Bb Am
Dm Dm Dm F Bb Am

e|---------------|---------------|
B|---------------|---------------|
G|---------------|---------------|
D|--0--3-----3---|--0--3-----3---|
A|--------3------|--------3------|
E|---------------|---------------|


[Verse 2]
Dm    Dm
Easy, easy
Dm               C
  You break the bridle to make
Bb     Am
Losing control
Dm    Dm
Easy, easy
Dm
 Crushed what you're holding
       Bb       Am
So you can say letting go is
Dm Am Dm
Easy, easy

[Interlude]

Dm Dm F Bb Am
Dm Dm Dm F Bb Am

e|---------------|---------------|
B|---------------|---------------|
G|---------------|---------------|
D|--0--3-----3---|--0--3-----3---|
A|--------3------|--------3------|
E|---------------|---------------|


[Verse 3]

Dm    Dm
Easy, easy
Dm               C
Burn all your things
             Bb      Am
To make the fight to forget
Dm   F    Dm
Easy, oh, easy
Dm       
Burn all your things
             Bb      Am
To make the fight to forget
Dm
Easy


[Interlude]

Dm Dm C Bb Am
Dm Dm Dm F Bb Am
Dm Dm C Bb Am
Dm Dm Dm F Bb Am

e|---------------|---------------|
B|---------------|---------------|
G|---------------|---------------|
D|--0--3-----3---|--0--3-----3---|
A|--------3------|--------3------|
E|---------------|---------------|


[Verse 1]

Dm    Dm
Easy, easy
Dm
Pull out your heart
             Bb   Am
To make the being alone
Dm    Dm
Easy, easy
Dm
Pull out your heart
             Bb   Am
To make the being alone
Dm Am Dm  F
Easy, easy

Dm Dm Dm Dm Dm 
Suggest correction
```


## Sportfreunde Stiller - Kompliment

[Back to Table of Contents](#user-content-table-of-contents)


```

[Intro]

D ; Am ; C ; Em


[Verse]

D                    Am
Wenn man so will bist du das Ziel einer langen Reise
        C                           Em  
die Perfektion der besten Art und Weise in stillen Momenten leise
D                   Am                      C                       Em
die Schaumkrone der Woge der Begeisterung bergauf mein Antrieb und Schwung


[Chorus]

 D             Am                         C                     Em
Ich wollte dir nur mal eben sagen dass du das Größte für mich bist
 D              Am                             C                   Em
und sicher gehn ob du denn das selbe für mich fühlst - für mich fühlst


[Verse 2]

D                     Am                            C
Wenn man so will bist du meine chill-out area meine Feiertage in jedem Jahr
      Em
meine Süßwarenabteilung im Supermarkt
                                       
D                               Am                                     C
die Lösung wenn mal was hakt so wertvoll das man es sich gerne auch spart
                     Em
und so schön das man nie darauf verzichten mag


[Chorus]

 D             Am                         C                     Em
Ich wollte dir nur mal eben sagen dass du das Größte für mich bist
 D              Am                             C                   Em
und sicher gehn ob du denn das selbe für mich fühlst - für mich fühlst
 D             Am                         C                     Em
Ich wollte dir nur mal eben sagen dass du das Größte für mich bist
 D              Am                             C                   Em
und sicher gehn ob du denn das selbe für mich fühlst - für mich fühlst

Ich hoffe die sind richtig so und ihr könnt was damit anfangen. :) Wenn nicht bitte Comment.
PS: habe keine in der Version gefunden. :)
```


## Tom Odell - Heal 2 półtony w dół

[Back to Table of Contents](#user-content-table-of-contents)


```

Gm       F       A#/D      D#
Take my mind and take my pain
         Gm     F     A#/D       D#
Like an empty bottle takes the rain
      Gm F/A      D#/A# A#     A#/D    D#
And    heal,       heal,    heal, heal

Gm      F        A#/D      D#
Take my past and take my sins
         Gm    F   A#/D        D#
Like an empty sail takes the wind
      Gm F/A      D#/A# A#     A#/D    D#
And    heal,       heal,    heal, heal

[Chorus]
             F     A#    D#/C
And tell me some things last
             F     A#    D#/C
And tell me some things last

[Verse]
Gm       F        A#/D      D#
Take my heart and take my hand
         Gm    F         A#/D   D#
Like an ocean takes the dirty sand
      Gm F/A      D#/A# A#     A#/D    D#
And    heal,       heal,    heal, heal

Gm       F       A#/D      D#
Take my mind and take my pain
         Gm    F     A#/D        D#
Like an empty bottle takes the rain
      Gm F/A      D#/A# A#     A#/D    D#
And    heal,       heal,    heal, heal

[Chorus]
             F    A#     D#/C
And tell me some things last
             F    A#     D#/C
And tell me some things last
```


## Tom Odell - Heal

[Back to Table of Contents](#user-content-table-of-contents)


```

Title: Heal 
Artist: Tom Odell 
Standard tuning 
Capo 1 

[Verse]
Am       G       C/E      F
Take my mind and take my pain
         Am     G     C/E       F
Like an empty bottle takes the rain
      Am G/B      F/C C     C/E    F
And    heal,       heal,    heal, heal

Am      G        C/E      F
Take my past and take my sins
         Am    G   C/E        F
Like an empty sail takes the wind
      Am G/B      F/C C     C/E    F
And    heal,       heal,    heal, heal

[Chorus]
             G     C    F/D
And tell me some things last 
             G     C    F/D
And tell me some things last

[Verse] 
Am       G        C/E      F
Take my heart and take my hand
         Am    G         C/E   F
Like an ocean takes the dirty sand
      Am G/B      F/C C     C/E    F
And    heal,       heal,    heal, heal

Am       G       C/E      F
Take my mind and take my pain 
         Am    G     C/E        F
Like an empty bottle takes the rain
      Am G/B      F/C C     C/E    F
And    heal,       heal,    heal, heal

[Chorus]  
             G    C     F/D
And tell me some things last 
             G    C     F/D
And tell me some things last
```


## Willamette Stone - I never wanted to go

[Back to Table of Contents](#user-content-table-of-contents)


```

[Verse]

D                            D/E D/F# G   
I spend these hollow nights, all  al--one 
D                                 D/E D/F# G
Sick from the looking glass, that you kept whole
D
Let's go

D                            D/E D/F# G
I saw the sea in you, it was a   mir--age
D                    D/E D/F# G         
It wasn't even so, I played the odds


[Pre-Chorus]

G - G/F# - Em     Bm               A         ( G - G/F# - Em sounds like G (E|--3--2--0--|) Em )
              I'm full of black and blues
Em      Bm                 A
   From all the nights with you


[Chorus]

D                 G
I never wanted to go
D                  G
I don't want you anymore
D                   G        Bm      A
I don't want you to feel the same as me
D                 G
I never wanted to go
D                       G
But now I'm letting you know
D                G        Bm      A     D
I just wanted to feel the life in me


[Verse]
 
D                           D/E D/F# G
I went from solid steel, to broken   glass
D                           D/E    D/F# G
The darkest side of me, you brought out fast


[Pre-Chorus]
                                       
G - G/F# - Em     Bm                 A
              The things I said were true, 
Em     Bm                    A
   I'm taking them back from you


[Chorus]

D                 G
I never wanted to go
D                  G
I don't want you anymore
D                   G        Bm      A
I don't want you to feel the same as me
D                 G
I never wanted to go
D                       G
But now I'm letting you know
D                G        Bm      A     D
I just wanted to feel the life in me


[Bridge]

G
I miss you
D
I wish you
Bm          A
Will let me be
G 
Just leave it
D
Stop breathing
A
I need to be free


[Chorus]

D                 G
I never wanted to go
D                  G
I don't want you anymore
D                   G        Bm      A
I don't want you to feel the same as me
D                 G
I never wanted to go
D                       G
But now I'm letting you know
D                G        Bm      A     D
I just wanted to feel the life in me

D                 G
I never wanted to go
D                  G
I don't want you anymore
D                   G        Bm      A
I don't want you to feel the same as me
D                 G
I never wanted to go
D                       G
But now I'm letting you know
D                G        Bm      A     D
I just wanted to feel the life in me

Please rate and comment! :) 
Suggest correction What's this?





```


