# Śpiewnik

## Jak dodać piosenkę?
Dodaj plik z piosenką w formacie .txt do folderu `txt`. Następnie wykonaj z konsoli:

```
bash generate.sh
```

## Gdzie jest śpiewnik?
W pliku `songbook.md`.
